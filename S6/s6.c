#include <stdio.h>

int main(void) {
    char c;
    int i = 0;

    while ((c = getchar()) != EOF) {
        if (c == '\n') {
            printf("OKT: %o HEX: %x\n", i, i);
            i = 0;
        }
        else if (c < '0' || c > '9') { // or use isdigit from <ctype.h>
            return 1;
        }
        else {
            i = (i * 10) + (c - '0');
        }
    }
    printf("\n"); // to move the prompt to the next line
    return 0;
}
