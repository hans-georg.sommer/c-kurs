#include <string.h>

int strend(const char *s, const char *t) {
    size_t len_s = strlen(s);
    size_t len_t = strlen(t);
    if (len_t > len_s) {
        // t is longer than s, no need to check further
        return 0;
    }
    const char* i = s + len_s - 1;
    const char* j = t + len_t - 1;
    while (j > t) {
        if (*i != *j) {
            return 0;
        }
        --i;
        --j;
    }
    return 1;
}

char* strrchr(const char* s, int c) {
    for (const char* i = s + strlen(s) - 1; i >= s; --i) {
        if (*i == c) {
            return i;
        }
    }
    return NULL;
}

char* strstr(const char* s, const char* t) {
    size_t len_s = strlen(s);
    size_t len_t = strlen(t);
    const char *j, *k;
    for (const char* i = s; i < s + len_s; ++i) {
        for (j = t, k = i; j < t + len_t && *j == *k && *j != '\0'; ++j, ++k) {
            ;
        }
        if (j == t + len_t - 1) {
            return i;
        }
    }
    return NULL;
}
