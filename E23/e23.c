#include <stdint.h>

int sudokucheck(const int** s) {
    // check if s contains invalid data
    for (int y = 0; y < 9; ++y) {
        for (int x = 0; x < 9; ++x) {
            if (s[y][x] < 1 || s[y][x] > 9) {
                return 0;
            }
        }
    }

    uint8_t arr[18];

    // check rows and columns at once
    for (int y = 0; y < 9; ++y) {
        memset(arr, 0, sizeof(arr));
        for (int x = 0; x < 9; ++x) {
            ++arr[s[y][x] - 1];
            ++arr[8 + s[y][x]];
        }
        for (int i = 0; i < 18; ++i) {
            if (arr[i] != 1) {
                return 0;
            }
        }
    }

    // check cells
    for (int y = 0; y < 9; y += 3) {
        for (int x = 0; x < 9; x += 3) {
            memset(arr, 0, sizeof(arr));
            for (int i = y; i < y + 3; ++i) {
                for (int j = x; j < x + 3; ++j) {
                    ++arr[s[i][j] - 1];
                }
            }
            for (int i = 0; i < 9; ++i) {
                if (arr[i] != 1) {
                    return 0;
                }
            }
        }
    }

    // everything ok
    return 1;
}
