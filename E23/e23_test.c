#include <CUnit/CUnit.h>
#include "CUnit/Basic.h"
#include "e23.c"

void test_sudokucheck(void){

// Correct sudoku

  int laenge=9;
  int matrix[9][9] = {  {5,3,4,6,7,8,9,1,2},
                        {6,7,2,1,9,5,3,4,8},
		        {1,9,8,3,4,2,5,6,7},
			{8,5,9,7,6,1,4,2,3},
			{4,2,6,8,5,3,7,9,1},
			{7,1,3,9,2,4,8,5,6},
			{9,6,1,5,3,7,2,8,4},
			{2,8,7,4,1,9,6,3,5},
			{3,4,5,2,8,6,1,7,9}};
  const int* m[9];
  for (int i=0; i<laenge; i++) m[i] = matrix[i];
  if (sudokucheck(m) == 1) {
    CU_PASS(matrix: {{5 3 4 6 7 8 9 1 2} {6 7 2 1 9 5 3 4 8} {1 9 8 3 4 2 5 6 7} {8 5 9 7 6 1 4 2 3} {4 2 6 8 5 3 7 9 1} {7 1 3 9 2 4 8 5 6} {9 6 1 5 3 7 2 8 4} {2 8 7 4 1 9 6 3 5} {3 4 5 2 8 6 1 7 9}});
  } else {
    CU_FAIL(matrix: {{5 3 4 6 7 8 9 1 2} {6 7 2 1 9 5 3 4 8} {1 9 8 3 4 2 5 6 7} {8 5 9 7 6 1 4 2 3} {4 2 6 8 5 3 7 9 1} {7 1 3 9 2 4 8 5 6} {9 6 1 5 3 7 2 8 4} {2 8 7 4 1 9 6 3 5} {3 4 5 2 8 6 1 7 9}} erwartet: 1);
  }

// Wrong value
  int matrix2[9][9] = { {5,3,4,6,7,8,9,1,2},
                        {6,7,2,1,9,5,3,4,8},
		        {1,9,8,3,4,2,5,6,7},
			{8,5,9,7,0,1,4,2,3},
			{4,2,6,8,5,3,7,9,1},
			{7,1,3,9,2,4,8,5,6},
			{9,6,1,5,3,7,2,8,4},
			{2,8,7,4,1,9,6,3,5},
			{3,4,5,2,8,6,1,7,9}};

  for (int i=0; i<laenge; i++) m[i] = matrix2[i];
  if (sudokucheck(m) == 0) {
    CU_PASS(matrix: {{5 3 4 6 7 8 9 1 2} {6 7 2 1 9 5 3 4 8} {1 9 8 3 4 3 5 6 7} {8 5 9 7 0 1 4 2 3} {4 2 6 8 5 3 7 9 1} {7 1 3 9 2 4 8 5 6} {9 6 1 5 3 7 2 8 4} {2 8 7 4 1 9 6 3 5} {3 4 5 2 8 6 1 7 9}});
  } else {
    CU_FAIL(matrix: {{5 3 4 6 7 8 9 1 2} {6 7 2 1 9 5 3 4 8} {1 9 8 3 4 3 5 6 7} {8 5 9 7 0 1 4 2 3} {4 2 6 8 5 3 7 9 1} {7 1 3 9 2 4 8 5 6} {9 6 1 5 3 7 2 8 4} {2 8 7 4 1 9 6 3 5} {3 4 5 2 8 6 1 7 9}} erwartet: 0);
  }

//Doubled value in rows
  int matrix3[9][9] = { {5,3,4,6,7,8,9,1,2},
                        {6,7,2,1,9,5,3,4,8},
		        {1,9,8,3,4,2,5,6,7},
			{8,5,9,7,6,1,4,2,3},
			{4,6,6,8,5,3,7,9,1},
			{7,1,3,9,2,4,8,5,6},
			{9,2,1,5,3,7,2,8,4},
			{2,8,7,4,1,9,6,3,5},
			{3,4,5,2,8,6,1,7,9}};

  for (int i=0; i<laenge; i++) m[i] = matrix3[i];
  if (sudokucheck(m) == 0) {
    CU_PASS(matrix: {{5 3 4 6 7 8 9 1 2} {6 7 2 1 9 5 3 4 8} {1 9 8 3 4 3 5 6 7} {8 5 9 7 6 1 4 2 3} {4 6 6 8 5 3 7 9 1} {7 1 3 9 2 4 8 5 6} {9 2 1 5 3 7 2 8 4} {2 8 7 4 1 9 6 3 5} {3 4 5 2 8 6 1 7 9}});
  } else {
    CU_FAIL(matrix: {{5 3 4 6 7 8 9 1 2} {6 7 2 1 9 5 3 4 8} {1 9 8 3 4 3 5 6 7} {8 5 9 7 6 1 4 2 3} {4 6 6 8 5 3 7 9 1} {7 1 3 9 2 4 8 5 6} {9 2 1 5 3 7 2 8 4} {2 8 7 4 1 9 6 3 5} {3 4 5 2 8 6 1 7 9}} erwartet: 0);
  }

//Doubled value in columns
  int matrix4[9][9] = { {5,3,4,6,7,8,9,1,2},
                        {6,7,2,1,9,5,3,4,8},
		        {1,9,8,3,4,2,5,6,7},
			{8,5,9,7,6,1,4,2,3},
			{4,2,7,8,5,3,6,9,1},
			{7,1,3,9,2,4,8,5,6},
			{9,6,1,5,3,7,2,8,4},
			{2,8,7,4,1,9,6,3,5},
			{3,4,5,2,8,6,1,7,9}};
  for (int i=0; i<laenge; i++) m[i] = matrix4[i];
  if (sudokucheck(m) == 0) {
    CU_PASS(matrix: {{5 3 4 6 7 8 9 1 2} {6 7 2 1 9 5 3 4 8} {1 9 8 3 4 3 5 6 7} {8 5 9 7 6 1 4 2 3} {4 2 7 8 5 3 6 9 1} {7 1 3 9 2 4 8 5 6} {9 6 1 5 3 7 2 8 4} {2 8 7 4 1 9 6 3 5} {3 4 5 2 8 6 1 7 9}});
  } else {
    CU_FAIL(matrix: {{5 3 4 6 7 8 9 1 2} {6 7 2 1 9 5 3 4 8} {1 9 8 3 4 3 5 6 7} {8 5 9 7 6 1 4 2 3} {4 2 7 8 5 3 6 9 1} {7 1 3 9 2 4 8 5 6} {9 6 1 5 3 7 2 8 4} {2 8 7 4 1 9 6 3 5} {3 4 5 2 8 6 1 7 9}} erwartet: 0);
  }

//Doubled value in blocks
  int matrix5[9][9] = { {5,3,4,6,7,8,9,1,2},
                        {6,4,2,1,9,5,3,7,8},
		        {1,9,8,3,4,2,5,6,7},
			{8,5,9,7,6,1,4,2,3},
			{4,2,6,8,5,3,7,9,1},
			{7,1,3,9,2,4,8,5,6},
			{9,6,1,5,3,7,2,8,4},
			{2,8,7,4,1,9,6,3,5},
			{3,7,5,2,8,6,1,4,9}};
  for (int i=0; i<laenge; i++) m[i] = matrix5[i];
  if (sudokucheck(m) == 0) {
    CU_PASS(matrix: {{5 3 4 6 7 8 9 1 2} {6 4 2 1 9 5 3 7 8} {1 9 8 3 4 3 5 6 7} {8 5 9 7 6 1 4 2 3} {4 2 6 8 5 3 7 9 1} {7 1 3 9 2 4 8 5 6} {9 6 1 5 3 7 2 8 4} {2 8 7 4 1 9 6 3 5} {3 7 5 2 8 6 1 4 9}});
  } else {
    CU_FAIL(matrix: {{5 3 4 6 7 8 9 1 2} {6 4 2 1 9 5 3 7 8} {1 9 8 3 4 3 5 6 7} {8 5 9 7 6 1 4 2 3} {4 2 6 8 5 3 7 9 1} {7 1 3 9 2 4 8 5 6} {9 6 1 5 3 7 2 8 4} {2 8 7 4 1 9 6 3 5} {3 7 5 2 8 6 1 4 9}} erwartet: 0);
  }
}

int init_suite1(void)
{
  return 0;
}

int clean_suite1(void)
{
  return 0;
}


void cleanup(){}

int main(){
 CU_pSuite pSuite = NULL;

/* initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* add a suite to the registry */
  pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
   }

   /* add the tests to the suite */
   /* NOTE - ORDER IS IMPORTANT - MUST TEST fread() AFTER fprintf() */
   if ((NULL == CU_add_test(pSuite, "test of sudokucheck", test_sudokucheck)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}
