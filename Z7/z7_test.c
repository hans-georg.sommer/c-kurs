#include <CUnit/CUnit.h>
#include "CUnit/Basic.h"
#include "z7.c"

void test_strinv(void){
  char Hallo[] = "Hallo";
  char Eisenbahn[] = "Eisenbahn";
  char lagerregal[] = "lagerregal";
  CU_ASSERT_EQUAL(strcmp(strinv(""),""),0);
  CU_ASSERT_EQUAL(strcmp(strinv(" ")," "),0);
  CU_ASSERT_EQUAL(strcmp(strinv(Hallo),"ollaH"),0);
  CU_ASSERT_EQUAL(strcmp(strinv(Eisenbahn),"nhabnesiE"),0);
  CU_ASSERT_EQUAL(strcmp(strinv(lagerregal),"lagerregal"),0);
}

void test_strconcat(void){
  CU_ASSERT_EQUAL(strcmp(strconcat("",""),""),0);
  CU_ASSERT_EQUAL(strcmp(strconcat("","test"),"test"),0);
  CU_ASSERT_EQUAL(strcmp(strconcat("test",""),"test"),0);
  CU_ASSERT_EQUAL(strcmp(strconcat("Hallo ", "Welt"),"Hallo Welt"),0);
  CU_ASSERT_EQUAL(strcmp(strconcat("Weg", "beschreibung"),"Wegbeschreibung"),0);
}

int init_suite1(void)
{
  return 0;
}

int clean_suite1(void)
{
  return 0;
}


void cleanup(){}

int main(){
 CU_pSuite pSuite = NULL;

/* initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* add a suite to the registry */
  pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
   }

   /* add the tests to the suite */
   /* NOTE - ORDER IS IMPORTANT - MUST TEST fread() AFTER fprintf() */
   if ((NULL == CU_add_test(pSuite, "test of strinv", test_strinv)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if ((NULL == CU_add_test(pSuite, "test of strconcat", test_strconcat)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}
