#include <stdlib.h>
#include <string.h>

char *strinv(const char *s) {
    size_t len = strlen(s);
    char* res = malloc(len+1);
    for (unsigned int i = 0; i < len; ++i) {
        res[i] = s[len - i - 1];
    }
    res[len] = 0;
    return res;
}

char *strconcat(const char *s, const char *t) {
    size_t len_s = strlen(s);
    size_t len_t = strlen(t);
    char* res = malloc(len_s + len_t + 1);
    strcpy(res, s);
    strcat(res, t);
    return res;
}
