int sequence(int x, int n) {
    if (n < 0) {
        return 0;
    }

    if (n == 0) {
        return x;
    }

    int x_prev = sequence(x, n - 1);
    if (x_prev == 1) {
        return 1;
    }

    if (x_prev % 2) {
        return 3 * x_prev + 1;
    }

    return x_prev / 2;
}
