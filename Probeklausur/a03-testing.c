#include <stdio.h>

int lex_sorted( const char* array[] );

int main() {
  char* text[3];

  // first run ---------------------------------------------
  text[0] = "hallo";
  text[1] = "text";
  text[2] = NULL;

  printf("data\n");
  for(int i=0; i < 3; i++)
    printf("  [%d]: \"%s\"\n", i, text[i]);

  printf("is ");
  if (!lex_sorted((const char**) text))
    printf("not ");
    
  printf("in ascending order\n\n");
  
  // second run --------------------------------------------
  text[0] = "text";
  text[1] = "hallo";
  text[2] = NULL;

  printf("data\n");
  for(int i=0; i < 3; i++)
    printf("  [%d]: \"%s\"\n", i, text[i]);

  printf("is ");
  if (!lex_sorted((const char**) text))
    printf("not ");
    
  printf("in ascending order\n");

  return 0;
}
