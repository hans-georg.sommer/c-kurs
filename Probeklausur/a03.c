#include <string.h>

int lex_sorted(const char* array[]) {
    int i = 1;
    while (array[i] != NULL) {
        if (strcmp(array[i], array[i-1]) < 0) {
            return 0;
        }
        ++i;
    }
    return 1;
}
