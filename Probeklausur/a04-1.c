#include "a04-testing.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

data_t* new_data(char *name, int number) {
    data_t* n = malloc(sizeof(data_t));
    if (n == NULL) {
        return NULL;
    }
    strcpy(n->name, name);
    n->number = number;
    // no need to calculate c % 256, overflow has the same effect
    char c = number;
    while (*name != '\0') {
        c += *name;
        ++name;
    }
    n->checksum = c;
    return n;
}

int main(void) {
    data_t* t = new_data("Test", 7);
    printf("name: %s, number: %d, checksum: %d\n", t->name, t->number, (unsigned char)t->checksum);
    return 0;
}
