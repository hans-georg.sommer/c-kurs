#include <ctype.h>
#include <string.h>

int strclean(char *t) {
    if (t == NULL) {
        return -1;
    }
    char *i, *j;
    for (i = t, j = t; *i != '\0'; ++i) {
        if (isprint(*i) && !isspace(*i)) {
            *j = *i;
            ++j;
        }
    }
    *j = '\0';
    return i - j;
}
