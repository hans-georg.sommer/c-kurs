#include <CUnit/CUnit.h>
#include "CUnit/Basic.h"
#include "z5.c"


void test_sortieren_sortiertes_Feld(void){
  int feld1[10] = {1,2,3,4,5,6,7,8,9,10};
  int mod_feld[10] = {1,2,3,4,5,6,7,8,9,10};
  int start, stop, tausch, sortiert;
  int* feld = feld1;
  start=0;
  stop=0;
  tausch = sortieren(feld+start, feld+stop+1);
  sortiert = 1;

  for (int i=0; i<10; i++) {
    if (feld[i] != mod_feld[i]) sortiert = 0;
  }

  if (tausch == 0 && sortiert > 0) {
    CU_PASS(Feld: 1 2 3 4 5 6 7 8 9 10 von: 0 bis: 1 Tausch: 0);
  } else {
    CU_FAIL(Feld: 1 2 3 4 5 6 7 8 9 10 von: 0 bis: 1 Feld nachher: 1 2 3 4 5 6 7 8 9 10 Tausch: 0);
  }

  int feld2[10] = {1,2,3,4,5,6,7,8,9,10};
  feld = feld2;
  start=3;
  stop=5;
  tausch = sortieren(feld+start, feld+stop+1);
  sortiert = 1;

  for (int i=0; i<10; i++) {
    if (feld[i] != mod_feld[i]) sortiert = 0;
  }

  if (tausch == 0 && sortiert > 0) {
    CU_PASS(Feld: 1 2 3 4 5 6 7 8 9 10 von: 3 bis: 6 Tausch: 0);
  } else {
    CU_FAIL(Feld: 1 2 3 4 5 6 7 8 9 10 von: 3 bis: 6 Feld nachher: 1 2 3 4 5 6 7 8 9 10 Tausch: 0);
  }

  int feld3[10] = {1,2,3,4,5,6,7,8,9,10};
  feld = feld3;
  start=6;
  stop=9;
  tausch = sortieren(feld+start, feld+stop+1);
  sortiert = 1;

  for (int i=0; i<10; i++) {
    if (feld[i] != mod_feld[i]) sortiert = 0;
  }

  if (tausch == 0 && sortiert > 0) {
    CU_PASS(Feld: 1 2 3 4 5 6 7 8 9 10 von: 6 bis: 10 Tausch: 0);
  } else {
    CU_FAIL(Feld: 1 2 3 4 5 6 7 8 9 10 von: 6 bis: 10 Feld nachher: 1 2 3 4 5 6 7 8 9 10 Tausch: 0);
  }

  int feld4[10] = {1,2,3,4,5,6,7,8,9,10};
  feld = feld4;
  start=0;
  stop=9;
  tausch = sortieren(feld+start, feld+stop+1);
  sortiert = 1;

  for (int i=0; i<10; i++) {
    if (feld[i] != mod_feld[i]) sortiert = 0;
  }

  if (tausch == 0 && sortiert > 0) {
    CU_PASS(Feld: 1 2 3 4 5 6 7 8 9 10 von: 0 bis: 10 Tausch: 0);
  } else {
    CU_FAIL(Feld: 1 2 3 4 5 6 7 8 9 10 von: 0 bis: 10 Feld nachher: 1 2 3 4 5 6 7 8 9 10 Tausch: 0);
  }
}

void test_sortieren_unsortiertes_Feld(void){
  int feld1[10] = {3,7,5,5,2,1,6,10,9,4};
  int mod_feld[10] = {3,7,5,5,2,1,6,10,9,4};
  int start, stop, tausch, sortiert;
  int* feld = feld1;
  start=0;
  stop=0;
  tausch = sortieren(feld+start, feld+stop+1);
  sortiert = 1;

  for (int i=0; i<10; i++) {
    if (feld[i] != mod_feld[i]) sortiert = 0;
  }

  if (tausch == 0 && sortiert > 0) {
    CU_PASS(Feld: 3 7 5 5 2 1 6 10 9 4 von: 0 bis: 1 Tausch: 0);
  } else {
    CU_FAIL(Feld: 3 7 5 5 2 1 6 10 9 4 von: 0 bis: 1 Feld nachher: 3 7 5 5 2 1 6 10 9 4 Tausch: 0);
  }

  int feld2[10] = {3,7,5,5,2,1,6,10,9,4};
  feld = feld2;
  start=2;
  stop=3;
  tausch = sortieren(feld+start, feld+stop+1);
  sortiert = 1;

  for (int i=0; i<10; i++) {
    if (feld[i] != mod_feld[i]) sortiert = 0;
  }

  if (tausch == 0 && sortiert > 0) {
    CU_PASS(Feld: 3 7 5 5 2 1 6 10 9 4 von: 2 bis: 4 Tausch: 0);
  } else {
    CU_FAIL(Feld: 3 7 5 5 2 1 6 10 9 4 von: 2 bis: 4 Feld nachher: 3 7 5 5 2 1 6 10 9 4 Tausch: 0);
  }

  int feld3[10] = {3,7,5,5,2,1,6,10,9,4};
  feld = feld3;
  int mod_feld2[10] = {3,7,5,2,5,1,6,10,9,4};
  start=3;
  stop=4;
  tausch = sortieren(feld+start, feld+stop+1);
  sortiert = 1;

  for (int i=0; i<10; i++) {
    if (feld[i] != mod_feld2[i]) sortiert = 0;
  }

  if (tausch == 1 && sortiert > 0) {
    CU_PASS(Feld: 3 7 5 5 2 1 6 10 9 4 von: 3 bis: 5 Tausch: 1);
  } else {
    CU_FAIL(Feld: 3 7 5 5 2 1 6 10 9 4 von: 3 bis: 5 Feld nachher: 3 7 5 2 5 1 6 10 9 4 Tausch: 1);
  }

  int feld4[10] = {3,7,5,5,2,1,6,10,9,4};
  feld = feld4;
  int mod_feld3[10] = {3,1,2,5,5,6,7,10,9,4};
  start=1;
  stop=6;
  tausch = sortieren(feld+start, feld+stop+1);
  sortiert = 1;

  for (int i=0; i<10; i++) {
    if (feld[i] != mod_feld3[i]) sortiert = 0;
  }

  if (tausch == 3 && sortiert > 0) {
    CU_PASS(Feld: 3 7 5 5 2 1 6 10 9 4 von: 1 bis: 7 Tausch: 3);
  } else {
    CU_FAIL(Feld: 3 7 5 5 2 1 6 10 9 4 von: 1 bis: 7 Feld nachher: 3 1 2 5 5 6 7 10 9 4 Tausch: 3);
  }

  int feld5[10] = {3,7,5,5,2,1,6,10,9,4};
  feld = feld5;
  int mod_feld4[10] = {1,2,3,4,5,5,6,7,9,10};
  start=0;
  stop=9;
  tausch = sortieren(feld+start, feld+stop+1);
  sortiert = 1;

  for (int i=0; i<10; i++) {
    if (feld[i] != mod_feld4[i]) sortiert = 0;
  }

  if (tausch == 7 && sortiert > 0) {
    CU_PASS(Feld: 3 7 5 5 2 1 6 10 9 4 von: 0 bis: 10 Tausch: 7);
  } else {
    CU_FAIL(Feld: 3 7 5 5 2 1 6 10 9 4 von: 0 bis: 10 Feld nachher: 1 2 3 4 5 5 6 7 9 10 Tausch: 7);
  }

}

int init_suite1(void)
{
  return 0;
}

int clean_suite1(void)
{
  return 0;
}


void cleanup(){}

int main(){
 CU_pSuite pSuite = NULL;

/* initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* add a suite to the registry */
  pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
   }

   /* add the tests to the suite */
   /* NOTE - ORDER IS IMPORTANT - MUST TEST fread() AFTER fprintf() */
   if ((NULL == CU_add_test(pSuite, "test of sortieren, Feld sortiert", test_sortieren_sortiertes_Feld)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if ((NULL == CU_add_test(pSuite, "test of sortieren, Feld unsortiert", test_sortieren_unsortiertes_Feld)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}
