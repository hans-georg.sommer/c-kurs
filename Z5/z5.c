int sortieren (int *von, int *bis) {
    int counter = 0;
    while (von < bis) {
        int* min = von;
        for (int* i = von; i < bis; ++i) {
            if (*min > *i) {
                min = i;
            }
        }
        if (min != von) {
            int temp = *von;
            *von = *min;
            *min = temp;
            ++counter;
        }
        ++von;
    }
    return counter;
}
