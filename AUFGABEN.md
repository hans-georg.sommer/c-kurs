Aufgaben C-Kurs WiSe 2017 (Stand 02.11.2017)
============================================


# Inhalt
*   [1 Einführung und Einstieg](#kapitel_1)
*   [2 Formatierte Ein- und Ausgabe](#kapitel_2)
*   [3 (optional) Geschichte und Hintergrund](#kapitel_3)
*   [4 Ausdrücke und Operatoren](#kapitel_4)
*   [5 Was sind Zeiger?](#kapitel_5)
*   [6 Kontrollstrukturen und Steuerung des Programmablaufs](#kapitel_6)
*   [7 Felder, Vektoren, Matrizen und Arrays](#kapitel_7)
*   [8 Stringhandling - Wie behandelt und verändert man Zeichenketten?](#kapitel_8)
*   [9 Funktionen und Unterfunktionen](#kapitel_9)
*   [10 Zeiger Teil 2](#kapitel_10)
*   [11 Speichermanagement - Der Heap und andere Speicher](#kapitel_11)
*   [12 Einfache Strukturen](#kapitel_12)
*   [13 (optional) Einführung Datenstrukturen](#kapitel_13)
*   [14 (optional) Module und Multi-File-Programming](#kapitel_14)


# 1 Einführung und Einstieg <a name="kapitel_1" />

Dieses Kapitel enhält bisher keine Aufgaben.


# 2 Formatierte Ein- und Ausgabe <a name="kapitel_2" />

## Hello World

Schreiben Sie ein Programm `hello_world.c`, das `Hello World!` ausgibt.


## E3 [Escape-Sequenzen]

Schreiben Sie ein Programm `e3.c`, das folgenden Text ausgibt:

`Er kam lässig heran und sagte nur "Na, wie geht's?".
Kommentare beginnen mit /* und enden mit */. Verwechseln Sie
das bitte nicht mit \* bzw. *\!`


## S4 [Ausgabe von Zahlwerten mit printf, Standardkonstanten]

Schreiben Sie ein Programm `s4.c`, das die Werte der Standardkonstanten (Standard−Headerdateien `limits.h` bzw. `float.h`) folgendermaßen ausgibt:

`CHAR_MIN:
CHAR_MAX:
INT_MIN:
INT_MAX:
FLT_MIN:
FLT_MAX:
DBL_MIN:
DBL_MAX:`


# 3 (optional) Geschichte und Hintergrund <a name="kapitel_3" />

Dieses Kapitel enhält bisher keine Aufgaben.


# 4 Ausdrücke und Operatoren <a name="kapitel_4" />

Dieses Kapitel enhält bisher keine Aufgaben.


# 5 Was sind Zeiger? <a name="kapitel_5" />

Dieses Kapitel enhält bisher keine Aufgaben.


# 6 Kontrollstrukturen und Steuerung des Programmablaufs <a name="kapitel_6" />

## S5

Schreiben Sie ein Programm `s5.c`, das alle Zeichen ausgibt, die "größer" oder gleich 'A' und "kleiner" oder gleich 'z' sind.


## S7

Schreiben Sie ein Programm `s7.c`, das eine nichtnegative ganze Zahl einliest, ihre einfache Quersumme iterativ berechnet und ausgibt.
Beispiel: Die Quersumme von 387 ist 18

Die Ausgabe soll folgendermaßen aussehen:

`Quersumme:` *Wert*

Wenn die Eingabe keine gültige Zahl ist, soll

`Fehler bei der Eingabe.`

ausgegeben werden.


# 7 Felder, Vektoren, Matrizen und Arrays <a name="kapitel_7" />

## S6


Schreiben Sie ein Programm `s6.c`. Es sollen wiederholt (bis zur Eingabe von `<Ctrl>-d`) nichtnegative Dezimalzahlen eingelesen und die Werte jeweils im Oktal- und Hexadezimalsystem ausgegeben werden.

Die Ausgabe soll wie folgt aussehen:

`OKT:` *oktaler Wert* `HEX:` *hexadezimaler Wert*

Bei einer ungültigen Eingabe soll das Programm beendet werden.


## E5

Schreiben Sie ein Programm `e5.c`, das die Koeffizienten eines Polynoms *p(x)* mit maximalen Grad 32, beginnend mit dem kleinsten Grad einliest. Die Eingabe der Koeffizienten soll mit Eingabe von `<Ctrl>-d` beendet werden. Anschließend soll der Benutzer mit

`Bitte Stellen zur Auswertung angeben`

zur Eingabe von Stellen (Gleitkommawerte) aufgefordert werden. Diese Stellen *a* sollen bis zum Eingabeende eingelesen und jeweils unmittelbar nach Eingabe der Stelle der Wert *p(a)* des Polynoms an dieser Stelle berechnet und mit

`Wert des Polynoms an der Stelle` *Stelle* `:` *Wert*

ausgegeben werden. Nutzen Sie für die Ausgabe der Stelle und des Wertes das Formatzeichen `%g`. Verwenden Sie zur Berechnung des Werts das Hornerschema. Beispiel:

*p(x)* = 5.1x<sup>3</sup> - 1.8x<sup>2</sup> - 0.02x + 17.3 = 17.3 + x(-0.02 + x(-1.8 + 5.1x)).


## S8

Schreiben Sie ein Programm `s8.c`, das eine beliebige Anzahl von Gleitkommazahlen einliest (bis zur Eingabe von `<Ctrl>-d`) und ihren Mittelwert ausgibt. Die Zahlen müssen dazu nicht alle gleichzeitig gespeichert werden, sondern können nacheinander eingelesen und bearbeitet werden.

Wenn die Eingabe keine gültige Zahl ist, soll

`Fehler bei der Eingabe.`

ausgegeben werden.

Geben Sie ihre Lösung im Format `%lf` mit `Lösung:` *Wert* aus.


## S9

Schreiben Sie ein Programm `s9.c`. Es sollen bis zu hundert ganze Zahlen eingelesen und in einem Feld abgespeichert werden. Dann soll der Inhalt des Feldes ausgegeben, das Feld sortiert, und der Inhalt des Feldes erneut ausgegeben werden.

Realisieren Sie "Sortieren durch direkte Auswahl": Wenn *n* Zahlen zu sortieren sind, bestimmt man zunächst das kleinste Element und vertauscht es mit dem, das an Position *1* steht. Dann bestimmt man von den Elementen an den Positionen *2,..., n* das kleinste Element und vertauscht es mit dem an Position *2*, usw.

Die Ausgabe soll wie folgt aussehen:

`Die zu sortierenden Zahlen:` *Zahl1 Zahl2 Zahl3*

`Die Zahlen sortiert:` *Zahl1 Zahl2 Zahl3*

Beim Programmstart soll der Benutzer folgendermaßen zur Eingabe der Zahlen aufgefordert werden:

`Bitte bis zu 100 ganze Zahlen eingeben:`

Wenn die Eingabe keine gültige Zahl ist, soll

`Fehler bei der Eingabe.`

ausgegeben werden.


## S12

Schreiben Sie ein Programm `s12.c`. Es soll ein Text von der Standardeingabe eingelesen und bestimmt werden, wie oft die einzelnen ASCII-Zeichen in ihm vorkommen. Geben Sie die Zeichen sortiert nach Ihrem ASCII-Code (in aufsteigender Folge) aus und nutzen Sie für jedes Zeichen eine neue Zeile. Graphische Zeichen `c` sollen in der Form:

`Zeichen :` *Anzahl Zeichen*

und Steuerzeichen als:

`<CTRL>` *Nummer des Zeichens* `:` *Anzahl Zeichen*

ausgegeben werden.


## Z2

Schreiben Sie ein Programm `z2.c`, das einen String, der auch Leerzeichen enthalten darf, einliest und mittels `Palindrom` oder `kein Palindrom` ausgibt, ob dieser String ein Palindrom ist oder nicht. In dieser Aufgabe soll nicht zwischen Groß- und Kleinschreibung unterschieden werden.

Ein Palindrom ist eine Zeichenkette, die vorwärts und rückwärts gelesen genau gleich ist. Beispiele: *Lagerregal, smart trams*


## Z1

Schreiben Sie ein Programm `z1.c`, das eine Binärzahl einliest und den entsprechenden Dezimalwert mit

`Dezimal:` *Wert*

ausgibt. Fehlerhafte Eingaben werden mit

`Fehler bei der Eingabe`

zurückgewiesen und das Programm beendet.


## S14

Schreiben Sie ein Programm `s14.c`. Zu bestimmen sind alle Primzahlen kleiner als 1000. Zwei Verfahren:

a) Durch Division wird geprüft, ob die Zahlen *n* (1 < *n* < 1000) einen Teiler *k* (1 < *k* < *n*) besitzen. Das kann man etwas abkürzen: Es genügt einen Teiler zu finden, der kleiner oder gleich *√n* ist.

b) Beim "Sieb des Eratosthenes" (Eratosthenes von Kyrene, griechischer Mathematiker um 225 v. Chr.) werden zunächst alle zu untersuchenden Zahlen (hier 2 bis 999) aufgeschrieben. Jeder Schritt des eigentlichen Algorithmus besteht aus drei Einzelschritten:

1. Die erste nicht weggestrichene Zahl wird gesucht.
2. Diese Zahl wird als Primzahl notiert.
3. Ihre Vielfachen werden weggestrichen.

Realisieren Sie beide Algorithmen möglichst effizient. Geben Sie dabei die Zahlen durch ein Leerzeichen getrennt aus. Für die Abgabe genügt es, eine der beiden Lösungen hochzuladen.


# 8 Stringhandling - Wie behandelt und verändert man Zeichenketten? <a name="kapitel_8" />

## S10

Schreiben Sie ein Programm `s10.c`, das zwei Zeilen als Strings einliest und den zweiten String an den ersten String anhängt und ausgibt. Die eingelesenen Strings sollen jeweils maximal 50 Zeichen (einschließlich abschließendem Nullbyte) lang sein. Ist dieses nicht der Fall, soll das Programm mit der Fehlermeldung

`Fehler bei der Eingabe.`

abgebrochen werden.


# 9 Funktionen und Unterfunktionen <a name="kapitel_9" />

Die im ILIAS als E6 und E18 hinterlegten Aufgaben entsprechen in etwa der Aufgabe [Z3](#z3), die auch im SSS verfügbar ist.


# 10 Zeiger Teil 2 <a name="kapitel_10" />

Dieses Kapitel enhält bisher keine Aufgaben.


# 11 Speichermanagement - Der Heap und andere Speicher <a name="kapitel_11" />

Dieses Kapitel enhält bisher keine Aufgaben.


# 12 Einfache Strukturen <a name="kapitel_12" />

Dieses Kapitel enhält bisher keine Aufgaben.


# 13 (optional) Einführung Datenstrukturen <a name="kapitel_13" />

Dieses Kapitel enhält bisher keine Aufgaben.


# 14 (optional) Module und Multi-File-Programming <a name="kapitel_14" />

Dieses Kapitel enhält bisher keine Aufgaben.


# SSS-Aufgaben ohne Zuordnung

Die Inhalte im ILIAS sind noch sehr unvollständig, wahrscheinlich werden diese Aufgaben dort zu einem späteren Zeitpunkt ergänzt.


## S15

Schreiben Sie eine rekursive Funktion

`unsigned long ggT (unsigned long a, unsigned long b);`

die den größten gemeinsamen Teiler der nichtnegativen ganzen Zahlen `a` und `b` berechnet. Verwenden Sie die Formel

![Bild Formeldefinition](images/formel_ggt.png)


## S17

Zu einer eingegebenen Jahreszahl soll bestimmt werden, ob es sich um ein Schaltjahr handelt, oder nicht. Realisieren Sie eine Funktion

`int schaltjahr(int jahr);`

die den Wert `0` liefert, falls das Argument keine Schaltjahreszahl ist und `1` sonst.

Der Gregorianische Kalender legt fest, dass jedes Jahr mit durch 4 teilbarer Jahreszahl ein Schaltjahr ist, außer wenn die Jahreszahl durch 100 teilbar ist. In diesem Fall handelt es sich nur dann um ein Schaltjahr, wenn die Jahreszahl auch durch 400 teilbar ist.


## S20

Schreiben Sie eine Funktion

`int datum2int(int tag, int monat)`

die ein Datum in den Tag im Jahr (z. B. der 11.2. ist der 42. Tag im Jahr) und eine Funktion

`void int2datum(int tage, int* ergebnis)`

die einen Tag im Jahr in ein Datum umrechnet. `ergebnis` ist ein Feld, in dem der erste Eintrag dem Tag und der zweite Eintrag dem Monat entsprechen soll. Gehen Sie dabei davon aus, dass der Februar immer 28 Tage hat.


## S21

Lassen Sie bei Aufgabe S20 in der Funktion `int2datum` auch exotische Eingaben zu wie in diesen Beispielen: Der 366. Tag im Jahr 1991 ist der 1.1.1992; der 0. Tag im Jahr 1992 ist der 31.12.1991; usw. Die Jahreszahl ist dabei irrelevant.


## S24

Realisieren Sie die folgenden Funktionen:

`int strend (const char *s, const char *t)`

liefert `1`, wenn der String `t` am Ende des String `s` steht, und `0` sonst.

`char *strrchr (const char *s, int c)`

liefert den Zeiger auf das letzte Vorkommen des Zeichens `c` im String `s` bzw. den `NULL`-Zeiger, wenn `c` in `s` nicht vorkommt.

`char *strstr (const char *s, const char *t)`

liefert den Zeiger auf das erste Vorkommen des String `t` im String `s` bzw. den `NULL`-Zeiger, wenn `t` in `s` nicht vorkommt.


## E16

Benutzen Sie den `sizeof`-Operator, um zu ermitteln, wieviel Bytes für Werte der Ganzzahltypen reserviert werden. Die Ausgabe soll wie folgt aussehen:

`char`: Größe
`short`: Größe
`int`: Größe
`long`: Größe

Ermitteln Sie dann rechnerisch (z. B. mittels `kcalc`) die größten darstellbaren Werte der einzelnen Ganzzahltypen und vergleichen Sie diese mit den Werten aus Aufgabe S4.

**Hinweis:** Bitte beachten Sie, dass die Größe des Datentyps von der Systemarchitektur abhängt und der Test daher bei Ihnen lokal trotz richtiger Lösung fehlschlagen kann.


## E21

Ein Permutation der Länge *n* ist eine Abbildung der Menge {1, 2, . . . , *n*} auf sich selbst. Programmieren und testen Sie eine Funktion

`int permtest( const unsigned int* p, int laenge)`

die von einem übergebenen Feld der Länge `laenge` testet, ob der Inhalt eine Permutation repräsentiert und in diesem Fall den Wert `1` zurückliefert und sonst `0`.


## E22

Eine Permutationsmatrix ist eine quadratische, ganzzahlige Matrix, die in jeder Zeile und jeder Spalte genau einen Eintrag 1 und sonst nur 0-Einträge hat. Programmieren Sie eine Funktion

`int permatcheck(const int** p, int laenge)`

die von einer durch ein zweidimensionales `int`-Feld der Größe `laenge` x `laenge` testet, ob es sich um eine Permutationsmatrix handelt. Geben Sie `1` zurück, falls ja, und `0` sonst.


## E23

Eine Sudoku-Matrix ist eine 9x9-Matrix, deren Einträge ganze Zahlen zwischen 1 und 9 sind und die weitere Bedingungen erfüllen. Zur Formulierung der Bedingungen betrachten wir die Matrix als Blockmatrix aus 9 Blöcken von 3x3-Matrizen, die durch die Zeilen bzw. Spalten 1 bis 3, 4 bis 6 und 7 bis 9 gebildet werden. Jede dieser 3x3-Matrizen bezeichnen im Weiteren als Block. Nun können wir die Bedingungen formulieren, die aus einer Matrix obiger Form eine Sudoku-Matrix machen:

*   Jede Zeile und jede Spalte gibt eine Permutation der Länge 9 an.
*   Jeder Block enthält jeden der Werte 1 bis 9 genau einmal.

Programmieren Sie eine Funktion

`int sudokucheck(const int** s)`

die von einer durch ein zweidimensionales `int`-Feld der Größe 9x9 testet, ob es sich um eine Sudoku-Matrix handelt. Geben Sie `1` zurück, falls ja, und `0` sonst.


## E29

Modifizieren Sie Ihre Lösung aus S21 so, dass auch das Jahr berücksichtig wird. Ergänzen Sie dazu die Übergabeparameter der beiden Funktionen:

`int datum2int(int tag, int monat, int jahr);`

`void int2datum(int tage, int jahr, int* ergebnis);`

Das Feld `ergebnis` soll nun drei Einträge enthalten, wovon der dritte dem Jahr entspricht.

**Tipp:** Verwenden Sie Ihre Lösung aus S17 wieder.


## Z3

Rufen Sie in der Konsole den Befehl `factor 144` auf. Die Ausgabe

`144: 2 2 2 2 3 3`

gibt die Primzahlzerlegung von 144 an. Programmieren Sie dies in C nach, um beliebige ganze Zahlen bis zur Größe `UINT_MAX` verarbeiten zu können. Die zu verarbeitende Zahl wird dem Programm auf der Kommandozeile übergeben. Schreiben Sie eine Hauptfunktion für den Prototypen

`int main(int argc, char *argv[]);`

Bei einer ungültigen Anzahl von übergebenen Argumenten soll

`Falsche Argumentzahl`

ausgegeben werden.


## Z4

Lösen Sie Aufgabe S24 nochmal, aber ohne in den Funktionen den `[]`-Operator zu verwenden, also nur mit Zeigerarithmetik.


## Z5

Schreiben Sie eine Funktion, die den in Aufgabe S9 beschriebenen Algorithmus "Sortieren durch direkte Auswahl" auf einem (Teil-)Feld von ganzen Zahlen umsetzt, mit folgendem Prototyp:

`int sortieren (int *von, int *bis);`

Dabei ist `von` ein Zeiger auf die erste Komponente und `bis` ein Zeiger **hinter** die letzte Komponente des zu sortierenden (Teil-)Feldes. Die Funktion liefert die Anzahl der umgespeicherten Werte zurück. Speichern Sie die Werte nur um, wenn es nötig ist. Benutzen Sie in der Funktion ausschließlich Zeigerarithmetik und nicht den `[]`-Operator.


## Z6

Schreiben Sie einen rekursive Funktion mit dem Prototypen

`int sum(int n);`

die folgende Definition realisiert

![Bild Formeldefinition](images/formel.png)


## Z7

Realisieren Sie die folgenden Funktionen:

`char *strinv(const char *s)`

liefert das Inverse des String `s`, d. h. den String `s` rückwärts gelesen.

`char *strconcat(const char *s, const char *t)`

liefert einen String, der eine Vereinigung der Strings `s` und `t` ist, d. h. dem Hintereinanderstellen der Strings `s` und `t` entspricht.


## Z8

Definieren Sie den Typ `bruch_t` als Struktur mit zwei `int`-Komponenten `zaehler` und `nenner`. Realisieren Sie für diesen Typ folgende Standardoperationen als Funktionen:

*   Eingabe eines Bruches

    `void bruchEingabe(bruch_t* bruch)`

*   Ausgabe eines Bruches

    `void bruchAusgabe(bruch_t* bruch)`

*   Addition zweier Brüche. Dabei wird der zweite Bruch auf den ersten Bruch aufaddiert.

    `void bruchAddition(bruch_t* bruch, bruch_t add)`

*   Subtraktion zweier Brüche. Dabei wird der zweite Bruch vom ersten Bruch wegsubtrahiert.

    `void bruchSubtraktion(bruch_t* bruch, bruch_t sub)`

*   Multiplikation zweier Brüche. Dabei wird der zweite Bruch auf den ersten Bruch aufmultipliziert.

    `void bruchMultiplikation(bruch_t* bruch, bruch_t mult)`

*   Division zweier Brüche. Dabei wird der zweite Bruch vom ersten Bruch wegdividiert.

    `void bruchDivision(bruch_t* bruch, bruch_t div)`

*   Berechnung des Quotienten

    `float bruchQuotient(bruch_t bruch)`

*   Erweitern um einen (als Parameter zu übergebenden) Faktor

    `void bruchErweitern(bruch_t* bruch, int faktor)`

*   Kürzen

    `void bruchKuerzen(bruch_t* bruch)`

Kürzen Sie bei der Addition/Subtraktion/Multiplikation/Division so weit wie möglich. Geben Sie ganze Zahlen immer als Eintel an, also 1/1, 2/1, usw.
