#include "s17.c"

// accumulated days of year including the respective month (jan = 1)
const int dpm_acc[13] = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365};
const int dpm_acc_leap[13] = {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366};

int datum2int(int tag, int monat, int jahr) {
    if (monat > 2) {
        return dpm_acc[monat-1] + tag + schaltjahr(jahr);
    }
    return dpm_acc[monat-1] + tag;
}

void int2datum(int tage, int jahr, int* ergebnis) {
    int doy;
    while (tage < 1) {
        doy = 365 + schaltjahr(jahr-1);
        tage += doy;
        --jahr;
    }
    doy = 365 + schaltjahr(jahr);
    while (tage > doy) {
        tage -= doy;
        ++jahr;
        doy = 365 + schaltjahr(jahr);
    }
    for (int m = 1; m <= 12; ++m) {
        if (m[(schaltjahr(jahr) ? dpm_acc_leap : dpm_acc)] >= tage) {
            ergebnis[2] = jahr;
            ergebnis[1] = m;
            --m;
            ergebnis[0] = tage - m[(schaltjahr(jahr) ? dpm_acc_leap : dpm_acc)];
            break;
        }
    }
}
