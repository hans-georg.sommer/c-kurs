#include <CUnit/CUnit.h>
#include "CUnit/Basic.h"
#include "e29.c"

void test_datum2int(void){
  CU_ASSERT_EQUAL(datum2int(1,1,1990),1);
  CU_ASSERT_EQUAL(datum2int(28,2,1990),59);
  CU_ASSERT_EQUAL(datum2int(1,3,1990),60);
  CU_ASSERT_EQUAL(datum2int(31,12,1990),365);
  CU_ASSERT_EQUAL(datum2int(1,1,2000),1);
  CU_ASSERT_EQUAL(datum2int(29,2,2000),60);
  CU_ASSERT_EQUAL(datum2int(1,3,2000),61);
  CU_ASSERT_EQUAL(datum2int(31,12,2000),366);
}

void test_int2datum(void){
  int ergebnis[3];
  int2datum(1,2000,ergebnis);
  if (ergebnis[0]==1 && ergebnis[1]==1 && ergebnis[2]==2000) {
   CU_PASS(tage=1 jahr=2000);
  }
  else {
    CU_FAIL(tage=1 jahr=2000 erwartet: 1.1.2000);
  }

  int2datum(60,2000,ergebnis);
  if (ergebnis[0]==29 && ergebnis[1]==2 && ergebnis[2]==2000) {
   CU_PASS(tage=60 jahr=2000);
  }
  else {
    CU_FAIL(tage=60 jahr=2000 erwartet: 29.2.2000);
  }

  int2datum(61,2000,ergebnis);
  if (ergebnis[0]==1 && ergebnis[1]==3 && ergebnis[2]==2000) {
   CU_PASS(tage=61 jahr=2000);
  }
  else {
    CU_FAIL(tage=61 jahr=2000 erwartet: 1.3.2000);
  }

  int2datum(366,2000,ergebnis);
  if (ergebnis[0]==31 && ergebnis[1]==12 && ergebnis[2]==2000) {
   CU_PASS(tage=366 jahr=2000);
  }
  else {
    CU_FAIL(tage=366 jahr=2000 erwartet: 31.12.2000);
  }

  int2datum(59,1900,ergebnis);
  if (ergebnis[0]==28 && ergebnis[1]==2 && ergebnis[2]==1900) {
   CU_PASS(tage=59 jahr=1900);
  }
  else {
    CU_FAIL(tage=59 jahr=1900 erwartet: 28.2.1900);
  }

  int2datum(60,1900,ergebnis);
  if (ergebnis[0]==1 && ergebnis[1]==3 && ergebnis[2]==1900) {
   CU_PASS(tage=60 jahr=1900);
  }
  else {
    CU_FAIL(tage=60 jahr=1900 erwartet: 1.3.1900);
  }

  int2datum(365,1900,ergebnis);
  if (ergebnis[0]==31 && ergebnis[1]==12 && ergebnis[2]==1900) {
   CU_PASS(tage=365 jahr=1900);
  }
  else {
    CU_FAIL(tage=365 jahr=1900 erwartet: 31.12.1900);
  }

  int2datum(366,1900,ergebnis);
  if (ergebnis[0]==1 && ergebnis[1]==1 && ergebnis[2]==1901) {
   CU_PASS(tage=366 jahr=1900);
  }
  else {
    CU_FAIL(tage=366 jahr=1900 erwartet: 31.12.1901);
  }

  int2datum(0,2000,ergebnis);
  if (ergebnis[0]==31 && ergebnis[1]==12 && ergebnis[2]==1999) {
   CU_PASS(tage=0 jahr=2000);
  }
  else {
    CU_FAIL(tage=0 jahr=2000 erwartet: 31.12.1999);
  }

  int2datum(-364,2000,ergebnis);
  if (ergebnis[0]==1 && ergebnis[1]==1 && ergebnis[2]==1999) {
   CU_PASS(tage=-364 jahr=2000);
  }
  else {
    CU_FAIL(tage=-364 jahr=2000 erwartet: 1.1.1999);
  }

  int2datum(-365,2000,ergebnis);
  if (ergebnis[0]==31 && ergebnis[1]==12 && ergebnis[2]==1998) {
   CU_PASS(tage=-365 jahr=2000);
  }
  else {
    CU_FAIL(tage=-365 jahr=2000 erwartet: 31.12.1998);
  }

  int2datum(-365,2001,ergebnis);
  if (ergebnis[0]==1 && ergebnis[1]==1 && ergebnis[2]==2000) {
   CU_PASS(tage=-365 jahr=2001);
  }
  else {
    CU_FAIL(tage=-365 jahr=2001 erwartet: 1.1.2000);
  }

  int2datum(-366,2001,ergebnis);
  if (ergebnis[0]==31 && ergebnis[1]==12 && ergebnis[2]==1999) {
   CU_PASS(tage=-366 jahr=2001);
  }
  else {
    CU_FAIL(tage=-366 jahr=2001 erwartet: 31.12.1999);
  }

  int2datum(-700, 2001, ergebnis);
  if (ergebnis[0]==31 && ergebnis[1]==1 && ergebnis[2]==1999) {
   CU_PASS(tage=-700);
  }
  else {
    CU_FAIL(tage=-700 erwartet: 31.1.1999);
  }

  int2datum(730,2001,ergebnis);
  if (ergebnis[0]==31 && ergebnis[1]==12 && ergebnis[2]==2002) {
   CU_PASS(tage=730 jahr=2001);
  }
  else {
    CU_FAIL(tage=730 jahr=2001 erwartet: 31.12.2002);
  }

  int2datum(731,2001,ergebnis);
  if (ergebnis[0]==1 && ergebnis[1]==1 && ergebnis[2]==2003) {
   CU_PASS(tage=731 jahr=2001);
  }
  else {
    CU_FAIL(tage=731 jahr=2001 erwartet: 1.1.2003);
  }

  int2datum(731,2000,ergebnis);
  if (ergebnis[0]==31 && ergebnis[1]==12 && ergebnis[2]==2001) {
   CU_PASS(tage=730 jahr=2000);
  }
  else {
    CU_FAIL(tage=731 jahr=2000 erwartet: 31.12.2001);
  }

  int2datum(732,2000,ergebnis);
  if (ergebnis[0]==1 && ergebnis[1]==1 && ergebnis[2]==2002) {
   CU_PASS(tage=732 jahr=2000);
  }
  else {
    CU_FAIL(tage=732 jahr=2000 erwartet: 1.1.2002);
  }
}

int init_suite1(void)
{
  return 0;
}

int clean_suite1(void)
{
  return 0;
}


void cleanup(){}

int main(){
 CU_pSuite pSuite = NULL;

/* initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* add a suite to the registry */
  pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
   }

   /* add the tests to the suite */
   /* NOTE - ORDER IS IMPORTANT - MUST TEST fread() AFTER fprintf() */
   if ((NULL == CU_add_test(pSuite, "test of datum2int", test_datum2int)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if ((NULL == CU_add_test(pSuite, "test of int2datum", test_int2datum)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }
   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}
