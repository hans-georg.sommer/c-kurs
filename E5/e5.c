#include <stdio.h>

int main(void) {
    const int count = 33;
    int i = 0;
    //~ float coefficients[count] = {0}; // compiler complains about that
    float coefficients[33] = {0};

    // read coefficients
    while (i < count && (scanf("%f", &coefficients[i])) != EOF) {
        ++i;
    }

    printf("%s\n", "Bitte Stellen zur Auswertung angeben");
    float x;
    while ((scanf("%f", &x)) != EOF) {
        float value = 0.0;
        // iterate in reverse order to apply horner's method
        for (int j = i - 1; j >= 0; --j) {
            value = value * x + coefficients[j];
        }
        printf("Wert des Polynoms an der Stelle %g: %g\n", x, value);
    }
    return 0;
}
