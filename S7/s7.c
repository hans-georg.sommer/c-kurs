#include <stdbool.h>
#include <stdio.h>

int main(void) {
    char c;
    int sum = 0;
    bool not_empty = false;

    while (1) {
        c = getchar();
        if (c == '\n' && not_empty) {
            printf("Quersumme: %d\n", sum);
            return 0;
        }
        else if (c < '0' || c > '9') { // or use isdigit from <ctype.h>
            printf("Fehler bei der Eingabe.\n");
            return 1;
        }
        else {
            sum += c - '0';
            not_empty = true;
        }
    }
}

// First naive solution. Handles negative integers, but accepts non-numeric chars.

//~ int digit_sum(int i) {
    //~ int sum = 0;
    //~ while (i > 0) {
        //~ sum += i % 10;
        //~ i /= 10;
    //~ }
    //~ return sum;
//~ }

//~ int main(void) {
    //~ int i;
    //~ scanf("%d", &i);

    //~ if (i < 0) {
        //~ printf("Fehler bei der Eingabe.\n");
    //~ }
    //~ else {
        //~ printf("%d %d\n", i, digit_sum(i));
    //~ }

    //~ return 0;
//~ }
