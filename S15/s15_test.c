#include <CUnit/CUnit.h>
#include "CUnit/Basic.h"
#include "s15.c"


void test_ggT(void){
CU_ASSERT_EQUAL(ggT(0,0),0);
CU_ASSERT_EQUAL(ggT(1,0),1);
CU_ASSERT_EQUAL(ggT(0,1),1);
CU_ASSERT_EQUAL(ggT(3,0),3);
CU_ASSERT_EQUAL(ggT(6,3),3);
CU_ASSERT_EQUAL(ggT(12,3),3);
CU_ASSERT_EQUAL(ggT(12,8),4);
CU_ASSERT_EQUAL(ggT(8,12),4);
CU_ASSERT_EQUAL(ggT(15,8),1);
CU_ASSERT_EQUAL(ggT(8,15),1);
}

int init_suite1(void)
{
  return 0;
}

int clean_suite1(void)
{
  return 0;
}


void cleanup(){}

int main(){
 CU_pSuite pSuite = NULL;

/* initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* add a suite to the registry */
  pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
   }

   /* add the tests to the suite */
   /* NOTE - ORDER IS IMPORTANT - MUST TEST fread() AFTER fprintf() */
   if ((NULL == CU_add_test(pSuite, "test of ggT", test_ggT)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}
