unsigned long ggT (unsigned long a, unsigned long b) {
    if (b == 0) {
        return a;
    }
    else
        return ggT(b, a % b);
}
