#include <ctype.h> // tolower()
#include <stdbool.h> // bool type
#include <stdio.h> // printf()

// NOTE: doesn't work for strings of arbitrary length
#define MAXSIZE 1024

bool is_palindrome(char *str, int length) {
    for (int i = 0; i < (length + 1) / 2; ++i) {
        //~ printf("%c %c\n", tolower(str[i]), str[length-i-1]);
        if (tolower(str[i]) != tolower(str[length-i-1])) {
            return false;
        }
    }
    return true;
}

int main(void) {
    int i = 0;
    char input[MAXSIZE];

    while((input[i] = getchar()) != EOF && input[i] != '\n') {
        ++i;
    }

    if (is_palindrome(input, i)) {
        printf("Palindrom\n");
    }
    else {
        printf("kein Palindrom\n");
    }

    return 0;
}
