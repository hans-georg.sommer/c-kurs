#include <float.h>
#include <limits.h>
#include <stdio.h>

int main(void) {
    printf("CHAR_MIN: %d\n", CHAR_MIN);
    printf("CHAR_MAX: %d\n", CHAR_MAX);
    printf("INT_MIN: %d\n", INT_MIN);
    printf("INT_MAX: %d\n", INT_MAX);

    /* Note: it's unclear from the description,
     * in which format the numbers should be printed.
     * Note: %e prints 6 decimal places by default,
     * the test seems to expect only 5... */
    printf("FLT_MIN: %.5e\n", FLT_MIN);
    printf("FLT_MAX: %.5e\n", FLT_MAX);
    printf("DBL_MIN: %.5e\n", DBL_MIN);
    printf("DBL_MAX: %.5e\n", DBL_MAX);
    return 0;
}
