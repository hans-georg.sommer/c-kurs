#include <stdio.h>

int main(void) {
    // "char" instead of "unsigned char" would work as well here
    for (unsigned char c = 'A'; c <= 'z'; c++) {
        putchar(c);
    }
    putchar('\n');
    return 0;
}
