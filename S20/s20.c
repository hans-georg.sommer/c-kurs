// accumulated days of year including the respective month (jan = 1)
const int dpm_acc[13] = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365};

int datum2int(int tag, int monat) {
    return dpm_acc[monat-1] + tag;
}

void int2datum(int tage, int* ergebnis) {
    for (int m = 1; m <= 12; ++m) {
        if (dpm_acc[m] >= tage) {
            ergebnis[1] = m;
            ergebnis[0] = tage - dpm_acc[m-1];
            break;
        }
    }
}
