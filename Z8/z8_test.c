#include <CUnit/CUnit.h>
#include "CUnit/Basic.h"
#include <stdlib.h>
#include "z8.c"


void test_bruchKuerzen(void){
  bruch_t *bruch;
  bruch = (bruch_t *)malloc(sizeof (bruch_t));
  (*bruch).zaehler = 1;
  (*bruch).nenner = 1;
  bruchKuerzen(bruch);
  if ((*bruch).zaehler==1 && (*bruch).nenner==1) {
    CU_PASS(1/1);
  } else {
    CU_FAIL(1/1 = 1/1);
  }

  (*bruch).zaehler = 2;
  (*bruch).nenner = 1;
  bruchKuerzen(bruch);
  if ((*bruch).zaehler==2 && (*bruch).nenner==1) {
    CU_PASS(2/1);
  } else {
    CU_FAIL(2/1 = 2/1);
  }

  (*bruch).zaehler = 1;
  (*bruch).nenner = 2;
  bruchKuerzen(bruch);
  if ((*bruch).zaehler==1 && (*bruch).nenner==2) {
    CU_PASS(1/2);
  } else {
    CU_FAIL(1/2 = 1/2);
  }

  (*bruch).zaehler = 54;
  (*bruch).nenner = 12;
  bruchKuerzen(bruch);
  if ((*bruch).zaehler==9 && (*bruch).nenner==2) {
    CU_PASS(54/12);
  } else {
    CU_FAIL(54/12 = 9/2);
  }

  (*bruch).zaehler = 12;
  (*bruch).nenner = 54;
  bruchKuerzen(bruch);
  if ((*bruch).zaehler==2 && (*bruch).nenner==9) {
    CU_PASS(12/54);
  } else {
    CU_FAIL(12/54 = 2/9);
  }
}

void test_bruchAddition(void){
  bruch_t *summand1;
  summand1 = (bruch_t *)malloc(sizeof (bruch_t));
  bruch_t summand2;
  (*summand1).zaehler = 1;
  (*summand1).nenner = 1;
  summand2.zaehler = 1;
  summand2.nenner = 1;
  bruchAddition(summand1,summand2);
  if ((*summand1).zaehler == 2 && (*summand1).nenner == 1) {
    CU_PASS(1/1 + 1/1);
  } else {
    CU_FAIL(1/1 + 1/1 = 2/1);
  }

  (*summand1).zaehler = 1;
  (*summand1).nenner = 2;
  bruchAddition(summand1,summand2);
  if ((*summand1).zaehler == 3 && (*summand1).nenner == 2) {
    CU_PASS(1/2 + 1/1);
  } else {
    CU_FAIL(1/2 + 1/1 = 3/2);
  }

  (*summand1).zaehler = 1;
  (*summand1).nenner = 2;
  summand2.zaehler = 1;
  summand2.nenner = 2;
  bruchAddition(summand1,summand2);
  if ((*summand1).zaehler == 1 && (*summand1).nenner == 1) {
    CU_PASS(1/2 + 1/2);
  } else {
    CU_FAIL(1/2 + 1/2 = 1/1);
  }

  (*summand1).zaehler = 2;
  (*summand1).nenner = 5;
  summand2.zaehler = 7;
  summand2.nenner = 10;
  bruchAddition(summand1,summand2);
  if ((*summand1).zaehler == 11 && (*summand1).nenner == 10) {
    CU_PASS(2/5 + 7/10);
  } else {
    CU_FAIL(2/5 + 7/10 = 11/10);
  }
}

void test_bruchSubtraktion(void){
  bruch_t *minuend;
  minuend = (bruch_t *)malloc(sizeof (bruch_t));
  bruch_t subtrahend;
  (*minuend).zaehler = 1;
  (*minuend).nenner = 1;
  subtrahend.zaehler = 1;
  subtrahend.nenner = 1;
  bruchSubtraktion(minuend,subtrahend);
  if ((*minuend).zaehler == 0 && (*minuend).nenner == 1) {
    CU_PASS(1/1 - 1/1);
  } else {
    CU_FAIL(1/1 - 1/1 = 0/1);
  }

  (*minuend).zaehler = 1;
  (*minuend).nenner = 1;
  subtrahend.zaehler = 1;
  subtrahend.nenner = 2;
  bruchSubtraktion(minuend,subtrahend);
  if ((*minuend).zaehler == 1 && (*minuend).nenner == 2) {
    CU_PASS(1/1 - 1/2);
  } else {
    CU_FAIL(1/1 - 1/2 = 1/2);
  }

  (*minuend).zaehler = 1;
  (*minuend).nenner = 2;
  subtrahend.zaehler = 1;
  subtrahend.nenner = 1;
  bruchSubtraktion(minuend,subtrahend);
  if (((*minuend).zaehler == -1 && (*minuend).nenner == 2) || ((*minuend).zaehler == 1 && (*minuend).nenner == -2)) {
    CU_PASS(1/2 - 1);
  } else {
    CU_FAIL(1/2 - 1 = -1/2);
  }

  (*minuend).zaehler = 7;
  (*minuend).nenner = 10;
  subtrahend.zaehler = 2;
  subtrahend.nenner = 5;
  bruchSubtraktion(minuend,subtrahend);
  if ((*minuend).zaehler == 3 && (*minuend).nenner == 10) {
    CU_PASS(7/10 - 2/5);
  } else {
    CU_FAIL(7/10 - 2/5 = 3/10);
  }
}

void test_bruchMultiplikation(void){
  bruch_t *faktor1;
  faktor1 = (bruch_t *)malloc(sizeof (bruch_t));
  bruch_t faktor2;
  (*faktor1).zaehler = 1;
  (*faktor1).nenner = 1;
  faktor2.zaehler = 1;
  faktor2.nenner = 1;
  bruchMultiplikation(faktor1,faktor2);
  if ((*faktor1).zaehler == 1 && (*faktor1).nenner == 1) {
    CU_PASS(1/1 * 1/1);
  } else {
    CU_FAIL(1/1 * 1/1 = 1/1);
  }

  (*faktor1).zaehler = 1;
  (*faktor1).nenner = 1;
  faktor2.zaehler = 1;
  faktor2.nenner = 2;
  bruchMultiplikation(faktor1,faktor2);
  if ((*faktor1).zaehler == 1 && (*faktor1).nenner == 2) {
    CU_PASS(1/1 * 1/2);
  } else {
    CU_FAIL(1/1 * 1/2 = 1/2);
  }

  (*faktor1).zaehler = 1;
  (*faktor1).nenner = 2;
  faktor2.zaehler = 1;
  faktor2.nenner = 1;
  bruchMultiplikation(faktor1,faktor2);
  if ((*faktor1).zaehler == 1 && (*faktor1).nenner == 2) {
    CU_PASS(1/2 * 1/1);
  } else {
    CU_FAIL(1/2 * 1/1 = 1/2);
  }

  (*faktor1).zaehler = 7;
  (*faktor1).nenner = 10;
  faktor2.zaehler = 2;
  faktor2.nenner = 5;
  bruchMultiplikation(faktor1,faktor2);
  if ((*faktor1).zaehler == 7 && (*faktor1).nenner == 25) {
    CU_PASS(7/10 * 2/5);
  } else {
    CU_FAIL(7/10 * 2/5 = 7/25);
  }
}

void test_bruchDivision(void){
  bruch_t *faktor1;
  faktor1 = (bruch_t *)malloc(sizeof (bruch_t));
  bruch_t faktor2;
  (*faktor1).zaehler = 1;
  (*faktor1).nenner = 1;
  faktor2.zaehler = 1;
  faktor2.nenner = 1;
  bruchDivision(faktor1,faktor2);
  if ((*faktor1).zaehler == 1 && (*faktor1).nenner == 1) {
    CU_PASS(1/1 / 1/1);
  } else {
    CU_FAIL(1/1 / 1/1 = 1/1);
  }

  (*faktor1).zaehler = 1;
  (*faktor1).nenner = 1;
  faktor2.zaehler = 1;
  faktor2.nenner = 2;
  bruchDivision(faktor1,faktor2);
  if ((*faktor1).zaehler == 2 && (*faktor1).nenner == 1) {
    CU_PASS(1/1 / 1/2);
  } else {
    CU_FAIL(1/1 / 1/2 = 2/1);
  }

  (*faktor1).zaehler = 1;
  (*faktor1).nenner = 2;
  faktor2.zaehler = 1;
  faktor2.nenner = 1;
  bruchDivision(faktor1,faktor2);
  if ((*faktor1).zaehler == 1 && (*faktor1).nenner == 2) {
    CU_PASS(1/2 / 1/1);
  } else {
    CU_FAIL(1/2 / 1/1 = 1/2);
  }

  (*faktor1).zaehler = 7;
  (*faktor1).nenner = 10;
  faktor2.zaehler = 2;
  faktor2.nenner = 5;
  bruchDivision(faktor1,faktor2);
  if ((*faktor1).zaehler == 7 && (*faktor1).nenner == 4) {
    CU_PASS(7/10 / 2/5);
  } else {
    CU_FAIL(7/10 / 2/5 = 7/4);
  }
}

void test_bruchErweitern(void){
  bruch_t *bruch;
  bruch = (bruch_t *)malloc(sizeof (bruch_t));
  (*bruch).zaehler = 1;
  (*bruch).nenner = 1;
  int faktor=0;
  bruchErweitern(bruch,faktor);
  if ((*bruch).zaehler == 0 && (*bruch).nenner == 0) {
    CU_PASS(1/1 erweitert um 0);
  } else {
    CU_FAIL(1/1 * 0 = 0/0);
  }

  (*bruch).zaehler = 1;
  (*bruch).nenner = 1;
  faktor=5;
  bruchErweitern(bruch,faktor);
  if ((*bruch).zaehler == 5 && (*bruch).nenner == 5) {
    CU_PASS(1/1 erweitert um 5);
  } else {
    CU_FAIL(1/1 erweitert um 5 = 5/5);
  }

  (*bruch).zaehler = 2;
  (*bruch).nenner = 3;
  faktor=2;
  bruchErweitern(bruch,faktor);
  if ((*bruch).zaehler == 4 && (*bruch).nenner == 6) {
    CU_PASS(2/3 erweitert um 2);
  } else {
    CU_FAIL(2/3 erweitert um 2 = 4/6);
  }

  (*bruch).zaehler = 2;
  (*bruch).nenner = 3;
  faktor=3;
  bruchErweitern(bruch,faktor);
  if ((*bruch).zaehler == 6 && (*bruch).nenner == 9) {
    CU_PASS(2/3 erweitert um 3);
  } else {
    CU_FAIL(2/3 erweitert um 3 = 6/9);
  }

  (*bruch).zaehler = 1;
  (*bruch).nenner = 10;
  faktor=5;
  bruchErweitern(bruch,faktor);
  if ((*bruch).zaehler == 5 && (*bruch).nenner == 50) {
    CU_PASS(1/10 erweitert um 5);
  } else {
    CU_FAIL(1/10 erweitert um 5 = 6/50);
  }

}

void test_bruchQuotient(void) {
  bruch_t bruch;
  bruch.zaehler = 0;
  bruch.nenner = 1;
  float quotient = bruchQuotient(bruch);
  float quotient2 = (float) bruch.zaehler / bruch.nenner;
  if (quotient == quotient2) {
    CU_PASS(0/1);
  } else {
    CU_FAIL(0/1 = 0);
  }

  bruch.zaehler = 1;
  bruch.nenner = 1;
  quotient = bruchQuotient(bruch);
  quotient2 = (float) bruch.zaehler / bruch.nenner;
  if (quotient == quotient2) {
    CU_PASS(1/1);
  } else {
    CU_FAIL(1/1 = 1);
  }

  bruch.zaehler = 2;
  bruch.nenner = 1;
  quotient = bruchQuotient(bruch);
  quotient2 = (float) bruch.zaehler / bruch.nenner;
  if (quotient == quotient2) {
    CU_PASS(2/1);
  } else {
    CU_FAIL(2/1 = 2);
  }

  bruch.zaehler = 1;
  bruch.nenner = 2;
  quotient = bruchQuotient(bruch);
  quotient2 = (float) bruch.zaehler / bruch.nenner;
  if (quotient == quotient2) {
    CU_PASS(1/2);
  } else {
    CU_FAIL(1/2 = 0.5);
  }

  bruch.zaehler = 3;
  bruch.nenner = -5;
  quotient = bruchQuotient(bruch);
  quotient2 = (float) bruch.zaehler / bruch.nenner;
  if (quotient == quotient2) {
    CU_PASS(3/-5);
  } else {
    CU_FAIL(3/-5 = -0.6);
  }
}

int init_suite1(void)
{
  return 0;
}

int clean_suite1(void)
{
  return 0;
}


void cleanup(){}

int main(){
 CU_pSuite pSuite = NULL;

/* initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* add a suite to the registry */
  pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
   }

   /* add the tests to the suite */
   /* NOTE - ORDER IS IMPORTANT - MUST TEST fread() AFTER fprintf() */
   if ((NULL == CU_add_test(pSuite, "test of bruchAddition", test_bruchAddition)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if ((NULL == CU_add_test(pSuite, "test of bruchKuerzen", test_bruchKuerzen)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if ((NULL == CU_add_test(pSuite, "test of bruchSubtraktion", test_bruchSubtraktion)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if ((NULL == CU_add_test(pSuite, "test of bruchMultiplikation", test_bruchMultiplikation)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if ((NULL == CU_add_test(pSuite, "test of bruchDivision", test_bruchDivision)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if ((NULL == CU_add_test(pSuite, "test of bruchErweitern", test_bruchErweitern)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if ((NULL == CU_add_test(pSuite, "test of bruchQuotient", test_bruchQuotient)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}
