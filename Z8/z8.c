#include <stdio.h>

//helper function
int ggT(int a, int b) {
    int r;
    while (b != 0) {
        r = a % b;
        a = b;
        b = r;
    }
    return a;
}

typedef struct {
    int zaehler;
    int nenner;
} bruch_t;

void bruchEingabe(bruch_t* bruch) {
    //TODO: implement later, not tested
    return;
}

void bruchAusgabe(bruch_t* bruch) {
    printf("%d/%d\n", bruch->zaehler, bruch->nenner);
}

void bruchErweitern(bruch_t* bruch, int faktor) {
    bruch->zaehler *= faktor;
    bruch->nenner *= faktor;
}

void bruchKuerzen(bruch_t* bruch) {
    int d = ggT(bruch->zaehler, bruch->nenner);
    bruch->zaehler /= d;
    bruch->nenner /= d;
}

void bruchAddition(bruch_t* bruch, bruch_t add) {
    if (bruch->nenner != add.nenner) {
        int t = bruch->nenner;
        bruchErweitern(bruch, add.nenner);
        bruchErweitern(&add, t);
    }
    bruch->zaehler += add.zaehler;
    bruchKuerzen(bruch);
}

void bruchSubtraktion(bruch_t* bruch, bruch_t sub) {
    if (bruch->nenner != sub.nenner) {
        int t = bruch->nenner;
        bruchErweitern(bruch, sub.nenner);
        bruchErweitern(&sub, t);
    }
    bruch->zaehler -= sub.zaehler;
    bruchKuerzen(bruch);
}

void bruchMultiplikation(bruch_t* bruch, bruch_t mult) {
    bruch->zaehler *= mult.zaehler;
    bruch->nenner *= mult.nenner;
    bruchKuerzen(bruch);
}

void bruchDivision(bruch_t* bruch, bruch_t div) {
    bruch->zaehler *= div.nenner;
    bruch->nenner *= div.zaehler;
    bruchKuerzen(bruch);
}

float bruchQuotient(bruch_t bruch) {
    return (float) bruch.zaehler / bruch.nenner;
}
