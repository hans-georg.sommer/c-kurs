#include <stdbool.h> // bool
#include <stdint.h> // uint16_t
#include <stdio.h> // printf

#define MAXVAL 1000

uint16_t PRECALC[168] = {2, 3, 5, 7, 11, 13, 17, 19,
        23, 29, 31, 37, 41, 43, 47, 53,
        59, 61, 67, 71, 73, 79, 83, 89,
        97, 101, 103, 107, 109, 113, 127, 131,
        137, 139, 149, 151, 157, 163, 167, 173,
        179, 181, 191, 193, 197, 199, 211, 223,
        227, 229, 233, 239, 241, 251, 257, 263,
        269, 271, 277, 281, 283, 293, 307, 311,
        313, 317, 331, 337, 347, 349, 353, 359,
        367, 373, 379, 383, 389, 397, 401, 409,
        419, 421, 431, 433, 439, 443, 449, 457,
        461, 463, 467, 479, 487, 491, 499, 503,
        509, 521, 523, 541, 547, 557, 563, 569,
        571, 577, 587, 593, 599, 601, 607, 613,
        617, 619, 631, 641, 643, 647, 653, 659,
        661, 673, 677, 683, 691, 701, 709, 719,
        727, 733, 739, 743, 751, 757, 761, 769,
        773, 787, 797, 809, 811, 821, 823, 827,
        829, 839, 853, 857, 859, 863, 877, 881,
        883, 887, 907, 911, 919, 929, 937, 941,
        947, 953, 967, 971, 977, 983, 991, 997};

void just_print_them() {
    for (int i = 0; i < 167; ++i) {
        printf("%d ", PRECALC[i]);
    }
    printf("%d\n", PRECALC[167]);
}

void primes_by_division() {
    int i, j;
    printf("2");
    for (i = 3; i <= MAXVAL; i += 2) {
        j = 3;
        // only check up to the square root
        while (j * j <= i) {
            if (i % j == 0) {
                break;
            }
            j += 2;
        }
        // re-check, which condition ended the loop
        // TODO: can this check be avoided without goto?
        if (j * j > i) {
            printf(" %d", i);
        }
    }
    printf("\n");
}

void eratosthenes() {
    // init sieve - ignore all even numbers
    int sieve_size = MAXVAL / 2;
    int i, j;
    bool sieve[sieve_size];
    for (i = 0; i < sieve_size; ++i) {
        sieve[i] = true;
    }
    // iterate through the sieve, but first output the first prime number
    printf("2");
    for (i = 3; i <= MAXVAL; i += 2) {
        if (sieve[i/2-1]) {
            printf(" %d", i);
        }
        j = i * i; // everything lower than the square is already filtered out
        while (j <= MAXVAL) {
            if (j % 2 == 1) {
                sieve[j/2-1] = false;
            }
            j += i;
        }
    }
    printf("\n");
}

int main(void) {
    //~ just_print_them();
    //~ eratosthenes();
    primes_by_division();
    return 0;
}
