#include <stdio.h>

int main(void) {
    double f;
    double sum = 0;
    int count = 0;
    int i;
    while ((i = scanf("%lf", &f)) != EOF) {
        if (i == 1) {
            sum += f;
            ++count;
        }
        else {
            printf("Fehler bei der Eingabe.\n");
            return 1;
        }
    }

    printf("Lösung: %lf\n", sum / count);
    return 0;
}
