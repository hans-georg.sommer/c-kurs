#include <stdio.h>
#include <stdlib.h>

// source: http://www.cplusplus.com/reference/cstdlib/qsort/
int compare (const void * a, const void * b)
{
    return (*(int*)a - *(int*)b);
}

int main(void) {
    int size = 100;
	int array[size];
    int count = 0;
    int i;

    while(count < size) {
        i = scanf("%d", &array[count]);
        if (i == EOF) {
            break;
        }
        else if (i == 1) {
            ++count;
        }
        else {
            printf("Fehler bei der Eingabe.\n");
            return 1;
        }
    }

    printf("Die zu sortierenden Zahlen:");
    // TODO: separate function: print_int_array(int a[], int size)
    for (int i = 0; i < count; ++i) {
        printf(" %d", array[i]);
    }
    printf("\n");

    // why reinvent the wheel?
    qsort(array, count, sizeof(int), compare);

    printf("Die Zahlen sortiert:");
    for (int i = 0; i < count; ++i) {
        printf(" %d", array[i]);
    }
    printf("\n");
	return 0;
}
