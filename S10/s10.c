#include <stdbool.h>
#include <stdio.h> // printf()
#include <string.h>

// NOTE: this exercise annoyed me a lot, so in the end I didn't bother
// to clean up the code or look for a better solution.

#define MAXLEN 50

bool omg_wtf(char* s, size_t len) {
    char c;
    size_t i = 0;
    while ((c = getchar()) != EOF) {
        if (c == '\n') {
            if (i == 0) {
                return false;
            }
            s[i] = '\0';
            return true;
        }
        if (i == len - 1) {
            break;
        }
        s[i] = c;
        ++i;
    }
    return false;
}

int main(void) {
    char first[MAXLEN], second[MAXLEN];

    if (!omg_wtf(first, MAXLEN) || !omg_wtf(second, MAXLEN)) {
        printf("Fehler bei der Eingabe\n");
        return 1;
    }

    char res[MAXLEN * 2];
    strcpy(res, first);
    strcat(res, second);
    printf("%s\n", res);
    return 0;
}
