int schaltjahr(int jahr) {
    if (jahr % 4 == 0 && (jahr % 100 != 0 || jahr % 400 == 0)) {
        return 1;
    }
    else {
        return 0;
    }
}
