#include <CUnit/CUnit.h>
#include "CUnit/Basic.h"
#include "e22.c"

void test_permatcheck_length_1(void){
  int laenge=1;
  int matrix[1][1] = {{1}};
  const int* m[1];
  for (int i=0; i<laenge; i++) m[i] = matrix[i];
  if (permatcheck(m,laenge) == 1) {
    CU_PASS(matrix: {{1}});
  } else {
    CU_FAIL(matrix: {{1}} erwartet: 1);
  }

  matrix[0][0] = 0;
  if (permatcheck(m,laenge) == 0) {
    CU_PASS(matrix: {{0}});
  } else {
    CU_FAIL(matrix: {{0}} erwartet: 0);
  }

}

void test_permatcheck_length_2(void){
  int laenge=2;
  int matrix[2][2] = {{1,0},{0,1}};
  const int* m[2];
  for (int i=0; i<laenge; i++) m[i] = matrix[i];
  if (permatcheck(m,laenge) == 1) {
    CU_PASS(matrix: {{1 0} {0 1}});
  } else {
    CU_FAIL(matrix: {{1 0} {0 1}} erwartet: 1);
  }

  matrix[0][0] = 0;
  matrix[0][1] = 1;
  matrix[1][0] = 1;
  matrix[1][1] = 0;
  if (permatcheck(m,laenge) == 1) {
    CU_PASS(matrix: {{0 1} {1 0}});
  } else {
    CU_FAIL(matrix: {{0 1} {1 0}} erwartet: 1);
  }

  matrix[0][0] = 0;
  matrix[0][1] = 0;
  matrix[1][0] = 0;
  matrix[1][1] = 0;
  if (permatcheck(m,laenge) == 0) {
    CU_PASS(matrix: {{0 0} {0 0}});
  } else {
    CU_FAIL(matrix: {{0 0} {0 0}} erwartet: 0);
  }

  matrix[0][0] = 1;
  matrix[0][1] = 1;
  matrix[1][0] = 1;
  matrix[1][1] = 1;
  if (permatcheck(m,laenge) == 0) {
    CU_PASS(matrix: {{1 1} {1 1}});
  } else {
    CU_FAIL(matrix: {{1 1} {1 1}} erwartet: 0);
  }

  matrix[0][0] = 1;
  matrix[0][1] = 1;
  matrix[1][0] = 0;
  matrix[1][1] = 1;
  if (permatcheck(m,laenge) == 0) {
    CU_PASS(matrix: {{1 1} {0 1}});
  } else {
    CU_FAIL(matrix: {{1 1} {0 1}} erwartet: 0);
  }

  matrix[0][0] = 0;
  matrix[0][1] = 0;
  matrix[1][0] = 1;
  matrix[1][1] = 1;
  if (permatcheck(m,laenge) == 0) {
    CU_PASS(matrix: {{0 0} {1 1}});
  } else {
    CU_FAIL(matrix: {{0 0} {1 1}} erwartet: 0);
  }

  matrix[0][0] = 0;
  matrix[0][1] = 0;
  matrix[1][0] = 0;
  matrix[1][1] = 1;
  if (permatcheck(m,laenge) == 0) {
    CU_PASS(matrix: {{0 0} {0 1}});
  } else {
    CU_FAIL(matrix: {{0 0} {0 1}} erwartet: 0);
  }

  matrix[0][0] = 1;
  matrix[0][1] = 0;
  matrix[1][0] = 1;
  matrix[1][1] = 0;
  if (permatcheck(m,laenge) == 0) {
    CU_PASS(matrix: {{1 0} {1 0}});
  } else {
    CU_FAIL(matrix: {{1 0} {1 0}} erwartet: 0);
  }

  matrix[0][0] = 2;
  matrix[0][1] = 0;
  matrix[1][0] = 0;
  matrix[1][1] = 2;
  if (permatcheck(m,laenge) == 0) {
    CU_PASS(matrix: {{2 0} {0 2}});
  } else {
    CU_FAIL(matrix: {{2 0} {0 2}} erwartet: 0);
  }
}

void test_permatcheck_length_10(void){
  int laenge=10;
  int matrix[10][10] = {{1,0,0,0,0,0,0,0,0,0},
                        {0,1,0,0,0,0,0,0,0,0},
			{0,0,1,0,0,0,0,0,0,0},
			{0,0,0,1,0,0,0,0,0,0},
			{0,0,0,0,1,0,0,0,0,0},
			{0,0,0,0,0,1,0,0,0,0},
			{0,0,0,0,0,0,1,0,0,0},
			{0,0,0,0,0,0,0,1,0,0},
			{0,0,0,0,0,0,0,0,1,0},
			{0,0,0,0,0,0,0,0,0,1}};
 const  int* m[10];
  for (int i=0; i<laenge; i++) m[i] = matrix[i];
  if (permatcheck(m,laenge) == 1) {
    CU_PASS(matrix: {{1 0 0 0 0 0 0 0 0 0} {0 1 0 0 0 0 0 0 0 0} {0 0 1 0 0 0 0 0 0 0} {0 0 0 1 0 0 0 0 0 0} {0 0 0 0 1 0 0 0 0 0} {0 0 0 0 0 1 0 0 0 0} {0 0 0 0 0 0 1 0 0 0} {0 0 0 0 0 0 0 1 0 0} {0 0 0 0 0 0 0 0 1 0} {0 0 0 0 0 0 0 0 0 1}});
  } else {
    CU_FAIL(matrix: {{1 0 0 0 0 0 0 0 0 0} {0 1 0 0 0 0 0 0 0 0} {0 0 1 0 0 0 0 0 0 0} {0 0 0 1 0 0 0 0 0 0} {0 0 0 0 1 0 0 0 0 0} {0 0 0 0 0 1 0 0 0 0} {0 0 0 0 0 0 1 0 0 0} {0 0 0 0 0 0 0 1 0 0} {0 0 0 0 0 0 0 0 1 0} {0 0 0 0 0 0 0 0 0 1}} erwartet: 1);
  }

  matrix[0][0] = 0;
  matrix[0][3] = 1;
  matrix[3][0] = 1;
  matrix[3][3] = 0;
  if (permatcheck(m,laenge) == 1) {
    CU_PASS(matrix: {{0 0 0 1 0 0 0 0 0 0} {0 1 0 0 0 0 0 0 0 0} {0 0 1 0 0 0 0 0 0 0} {1 0 0 0 0 0 0 0 0 0} {0 0 0 0 1 0 0 0 0 0} {0 0 0 0 0 1 0 0 0 0} {0 0 0 0 0 0 1 0 0 0} {0 0 0 0 0 0 0 1 0 0} {0 0 0 0 0 0 0 0 1 0} {0 0 0 0 0 0 0 0 0 1}});
  } else {
    CU_FAIL(matrix: {{0 0 0 1 0 0 0 0 0 0} {0 1 0 0 0 0 0 0 0 0} {0 0 1 0 0 0 0 0 0 0} {1 0 0 0 0 0 0 0 0 0} {0 0 0 0 1 0 0 0 0 0} {0 0 0 0 0 1 0 0 0 0} {0 0 0 0 0 0 1 0 0 0} {0 0 0 0 0 0 0 1 0 0} {0 0 0 0 0 0 0 0 1 0} {0 0 0 0 0 0 0 0 0 1}} erwartet: 1);
  }

  matrix[9][8] = 1;
  if (permatcheck(m,laenge) == 0) {
    CU_PASS(matrix: {{0 0 0 1 0 0 0 0 0 0} {0 1 0 0 0 0 0 0 0 0} {0 0 1 0 0 0 0 0 0 0} {1 0 0 0 0 0 0 0 0 0} {0 0 0 0 1 0 0 0 0 0} {0 0 0 0 0 1 0 0 0 0} {0 0 0 0 0 0 1 0 0 0} {0 0 0 0 0 0 0 1 0 0} {0 0 0 0 0 0 0 0 1 0} {0 0 0 0 0 0 0 0 0 1 1}});
  } else {
    CU_FAIL(matrix: {{0 0 0 1 0 0 0 0 0 0} {0 1 0 0 0 0 0 0 0 0} {0 0 1 0 0 0 0 0 0 0} {1 0 0 0 0 0 0 0 0 0} {0 0 0 0 1 0 0 0 0 0} {0 0 0 0 0 1 0 0 0 0} {0 0 0 0 0 0 1 0 0 0} {0 0 0 0 0 0 0 1 0 0} {0 0 0 0 0 0 0 0 1 0} {0 0 0 0 0 0 0 0 0 1 1}} erwartet: 0);
  }

  matrix[9][8] = 0;
  matrix[4][4] = 0;
  if (permatcheck(m,laenge) == 0) {
    CU_PASS(matrix: {{0 0 0 1 0 0 0 0 0 0} {0 1 0 0 0 0 0 0 0 0} {0 0 1 0 0 0 0 0 0 0} {1 0 0 0 0 0 0 0 0 0} {0 0 0 0 0 0 0 0 0 0} {0 0 0 0 0 1 0 0 0 0} {0 0 0 0 0 0 1 0 0 0} {0 0 0 0 0 0 0 1 0 0} {0 0 0 0 0 0 0 0 1 0} {0 0 0 0 0 0 0 0 0 0 1}});
  } else {
    CU_FAIL(matrix: {{0 0 0 1 0 0 0 0 0 0} {0 1 0 0 0 0 0 0 0 0} {0 0 1 0 0 0 0 0 0 0} {1 0 0 0 0 0 0 0 0 0} {0 0 0 0 0 0 0 0 0 0} {0 0 0 0 0 1 0 0 0 0} {0 0 0 0 0 0 1 0 0 0} {0 0 0 0 0 0 0 1 0 0} {0 0 0 0 0 0 0 0 1 0} {0 0 0 0 0 0 0 0 0 0 1}} erwartet: 0);
  }
}

int init_suite1(void)
{
  return 0;
}

int clean_suite1(void)
{
  return 0;
}


void cleanup(){}

int main(){
 CU_pSuite pSuite = NULL;

/* initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* add a suite to the registry */
  pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
   }

   /* add the tests to the suite */
   /* NOTE - ORDER IS IMPORTANT - MUST TEST fread() AFTER fprintf() */
   if ((NULL == CU_add_test(pSuite, "test of permatcheck, length 1", test_permatcheck_length_1)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if ((NULL == CU_add_test(pSuite, "test of permatcheck, length 2", test_permatcheck_length_2)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if ((NULL == CU_add_test(pSuite, "test of permatcheck, length 10", test_permatcheck_length_10)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}
