#include <stdint.h>
#include <string.h>

int permatcheck(const int** p, int laenge) {
    uint8_t checked[laenge * 2];
    memset(checked, 0, laenge * 2);
    for (int x = 0; x < laenge; ++x) {
        for (int y = 0; y < laenge; ++y) {
            if (p[x][y] == 1) {
                if (checked[x] == 1 || checked[laenge + y] == 1) {
                    return 0;
                }
                checked[x] = 1;
                checked[laenge + y] = 1;
            }
            else if (p[x][y] != 0) {
                return 0;
            }
        }
    }
    for (int i = 0; i < laenge * 2; ++i) {
        if (checked[i] != 1) {
            return 0;
        }
    }
    return 1;
}
