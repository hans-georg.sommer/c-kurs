#include <stdint.h>

int permtest(const unsigned int* p, int laenge) {
    uint8_t count[laenge];
    for (int i = 0; i < laenge; ++i) {
        count[i] = 0;
    }
    unsigned int val;
    for (int i = 0; i < laenge; ++i) {
        val = p[i];
        if (val < 1 || val > laenge || count[val-1] > 0) {
            return 0;
        }
        ++count[val-1];
    }
    return 1;
}
