int sum(int n) {
    if (n == 0) {
        return 0;
    }
    return (n < 0) ? sum(n + 1) + n : sum(n - 1) + n;
}
