#include <CUnit/CUnit.h>
#include "CUnit/Basic.h"
#include "z6.c"


void test_sum(void){
CU_ASSERT_EQUAL(sum(0),0);
CU_ASSERT_EQUAL(sum(1),1);
CU_ASSERT_EQUAL(sum(-1),-1);
CU_ASSERT_EQUAL(sum(2),3);
CU_ASSERT_EQUAL(sum(-2),-3);
CU_ASSERT_EQUAL(sum(10),55);
CU_ASSERT_EQUAL(sum(-10),-55);
}

int init_suite1(void)
{
  return 0;
}

int clean_suite1(void)
{
  return 0;
}


void cleanup(){}

int main(){
 CU_pSuite pSuite = NULL;

/* initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* add a suite to the registry */
  pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
   }

   /* add the tests to the suite */
   /* NOTE - ORDER IS IMPORTANT - MUST TEST fread() AFTER fprintf() */
   if ((NULL == CU_add_test(pSuite, "test of sum", test_sum)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}
