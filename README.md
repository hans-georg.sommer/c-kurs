Meine Lösungen für die Aufgaben des C-Kurses (Online-Version) im WiSe 2017/18.

Work in progress - Vollständigkeit und Korrektheit werden nicht garantiert!

# Abhängigkeiten
* gcc
* make
* CUnit (unter Debian/Ubuntu: `sudo apt install libcunit1-dev`)

# Aufgabenstellungen

Siehe [Aufgabenstellungen](AUFGABEN.md).
