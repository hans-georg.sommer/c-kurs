#include <stdio.h>

int main(void) {
    int three = 3;
    float pi = 3.141593;

    printf("Aller guten Dinge sind %d\n", three);
    printf("Pi ist ungefaehr %f\n", pi);

    return 0;
}
