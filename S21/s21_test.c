#include <CUnit/CUnit.h>
#include "CUnit/Basic.h"
#include "s21.c"

void test_int2datum(void){
  int ergebnis[2];
  int2datum(1,ergebnis);
  if (ergebnis[0]==1 && ergebnis[1]==1) {
   CU_PASS(tage=1);
  }
  else {
    CU_FAIL(tage=1 erwartet: 1.1.);
  }

  int2datum(31,ergebnis);
  if (ergebnis[0]==31 && ergebnis[1]==1) {
   CU_PASS(tage=31);
  }
  else {
    CU_FAIL(tage=31 erwartet: 31.1.);
  }

  int2datum(32,ergebnis);
  if (ergebnis[0]==1 && ergebnis[1]==2) {
   CU_PASS(tage=32);
  }
  else {
    CU_FAIL(tage=32 erwartet: 1.2.);
  }

  int2datum(60,ergebnis);
  if (ergebnis[0]==1 && ergebnis[1]==3) {
   CU_PASS(tage=60);
  }
  else {
    CU_FAIL(tage=60 erwartet: 1.3.);
  }

  int2datum(277,ergebnis);
  if (ergebnis[0]==4 && ergebnis[1]==10) {
   CU_PASS(tage=277);
  }
  else {
    CU_FAIL(tage=277 erwartet: 4.10.);
  }

  int2datum(365,ergebnis);
  if (ergebnis[0]==31 && ergebnis[1]==12) {
   CU_PASS(tage=365);
  }
  else {
    CU_FAIL(tage=365 erwartet: 31.12.);
  }

  int2datum(0,ergebnis);
  if (ergebnis[0]==31 && ergebnis[1]==12) {
   CU_PASS(tage=0);
  }
  else {
    CU_FAIL(tage=0 erwartet: 31.12.);
  }
  int2datum(-364,ergebnis);
  if (ergebnis[0]==1 && ergebnis[1]==1) {
   CU_PASS(tage=-364);
  }
  else {
    CU_FAIL(tage=-364 erwartet: 1.1.);
  }
  int2datum(0,ergebnis);
  if (ergebnis[0]==31 && ergebnis[1]==12) {
   CU_PASS(tage=-365);
  }
  else {
    CU_FAIL(tage=-365 erwartet: 31.12.);
  }
  int2datum(-700,ergebnis);
  if (ergebnis[0]==30 && ergebnis[1]==1) {
   CU_PASS(tage=-700);
  }
  else {
    CU_FAIL(tage=-700 erwartet: 30.1.);
  }
  int2datum(366,ergebnis);
  if (ergebnis[0]==1 && ergebnis[1]==1) {
   CU_PASS(tage=366);
  }
  else {
    CU_FAIL(tage=366 erwartet: 1.1.);
  }
  int2datum(730,ergebnis);
  if (ergebnis[0]==31 && ergebnis[1]==12) {
   CU_PASS(tage=730);
  }
  else {
    CU_FAIL(tage=730 erwartet: 31.12.);
  }
  int2datum(731,ergebnis);
  if (ergebnis[0]==1 && ergebnis[1]==1) {
   CU_PASS(tage=731);
  }
  else {
    CU_FAIL(tage=731 erwartet: 1.1.);
  }
  int2datum(800,ergebnis);
  if (ergebnis[0]==11 && ergebnis[1]==3) {
   CU_PASS(tage=800);
  }
  else {
    CU_FAIL(tage=800 erwartet: 11.3.);
  }

}

int init_suite1(void)
{
  return 0;
}

int clean_suite1(void)
{
  return 0;
}


void cleanup(){}

int main(){
 CU_pSuite pSuite = NULL;

/* initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* add a suite to the registry */
  pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
   }

   /* add the tests to the suite */
   /* NOTE - ORDER IS IMPORTANT - MUST TEST fread() AFTER fprintf() */

   if ((NULL == CU_add_test(pSuite, "test of int2datum", test_int2datum)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }
   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}
