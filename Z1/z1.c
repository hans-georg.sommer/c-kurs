#include <stdio.h> // printf()

int main(void) {
    int num = 0;
    char c;

    while((c = getchar()) != EOF && c != '\n') {
        if (c != '0' && c != '1') {
            printf("Fehler bei der Eingabe\n");
            return 1;
        }
        num = c == '0' ? num * 2: num * 2 + 1;
    }

    printf("Dezimal: %d\n", num);
    return 0;
}
