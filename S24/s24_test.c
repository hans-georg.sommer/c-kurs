#include <CUnit/CUnit.h>
#include "CUnit/Basic.h"
#include "s24.c"

void test_strend(void){
  CU_ASSERT_EQUAL(strend("",""),1);
  CU_ASSERT_EQUAL(strend(" ",""),1);
  CU_ASSERT_EQUAL(strend(""," "),0);
  CU_ASSERT_EQUAL(strend("Hallo","o"),1);
  CU_ASSERT_EQUAL(strend("o","Hallo"),0);
  CU_ASSERT_EQUAL(strend("Hallo","Hallo"),1);
  CU_ASSERT_EQUAL(strend("Eisenbahn","bahn"),1);
  CU_ASSERT_EQUAL(strend("Eisenbahn","bah"),0);
  CU_ASSERT_EQUAL(strend("Eisenbahnwaggon","bahn"),0);
}

void test_strrchr(void){
  char empty[1] = "\0";
  char hallo[5] = "Hallo";
  char *emptyString = empty;
  char *Hallo = hallo;
  char *lo = &hallo[3];
  char *o = &hallo[4];
  char *Hallo2 = &hallo[0];
  CU_ASSERT_EQUAL(strrchr(emptyString,'a'),NULL);
  CU_ASSERT_EQUAL(strrchr(Hallo,'H'),Hallo2);
  CU_ASSERT_EQUAL(strrchr(Hallo,'o'),o);
  CU_ASSERT_EQUAL(strrchr(Hallo,'l'),lo);
  CU_ASSERT_EQUAL(strrchr(Hallo,'h'),NULL);
  CU_ASSERT_EQUAL(strrchr(Hallo,'b'),NULL);
}

void test_strstr(void){
  char *hallo = "Hallo";
  char *lo = &hallo[3];
  char *o = &hallo[4];
  char *Hallo = &hallo[0];
  char *allo = &hallo[1];

  char empty[1] = "\0";
  char *emptyString = empty;

  char *Eisenbahn = "Eisenbahn";
  char *bahn = &Eisenbahn[5];

  char *Eisenbahnwaggon = "Eisenbahnwaggon";
  char *bahnwaggon = &Eisenbahnwaggon[5];

  char *Strassenbahngleisenreinigungsbahn = "Strassenbahngleisenreinigungsbahn";
  char *bahngleisenreinigungsbahn = &Strassenbahngleisenreinigungsbahn[8];

  CU_ASSERT_EQUAL(strstr(emptyString,""),emptyString);
  CU_ASSERT_EQUAL(strstr(emptyString," "),NULL);
  CU_ASSERT_EQUAL(strstr(Hallo,"all"),allo);
  CU_ASSERT_EQUAL(strstr(o,"Hallo"),NULL);
  CU_ASSERT_EQUAL(strstr(Hallo,"Hallo"),Hallo);
  CU_ASSERT_EQUAL(strstr(Eisenbahn,"bahn"),bahn);
  CU_ASSERT_EQUAL(strstr(Eisenbahn,"bah"),bahn);
  CU_ASSERT_EQUAL(strstr(Eisenbahnwaggon,"sense"),NULL);
  CU_ASSERT_EQUAL(strstr(Strassenbahngleisenreinigungsbahn,"bahn"),bahngleisenreinigungsbahn);
}

int init_suite1(void)
{
  return 0;
}

int clean_suite1(void)
{
  return 0;
}


void cleanup(){}

int main(){
 CU_pSuite pSuite = NULL;

/* initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* add a suite to the registry */
  pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
   }

   /* add the tests to the suite */
   /* NOTE - ORDER IS IMPORTANT - MUST TEST fread() AFTER fprintf() */
   if ((NULL == CU_add_test(pSuite, "test of strend", test_strend)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if ((NULL == CU_add_test(pSuite, "test of strrchr", test_strrchr)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if ((NULL == CU_add_test(pSuite, "test of strstr", test_strstr)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}
