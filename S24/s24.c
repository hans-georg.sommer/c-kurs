#include <string.h>

int strend(const char *s, const char *t) {
    int i = strlen(s);
    int j = strlen(t);
    if (j > i) {
        // t is longer than s, no need to check further
        return 0;
    }
    for (; j > 0; --i, --j) {
        if (s[i-1] != t[j-1]) {
            return 0;
        }
    }
    return 1;
}

// There is no need to implement these functions, since they are part of <string.h>.
// So my own solutions are commented out...

//~ char *strrchr(const char *s, int c) {
    //~ for (int i = strlen(s) - 1; i >= 0; --i) {
        //~ if (c == s[i]) {
            //~ return &s[i];
        //~ }
    //~ }
    //~ return NULL;
//~ }

//~ char *strstr(const char *s, const char *t) {
    //~ int len_s = strlen(s);
    //~ int len_t = strlen(t);
    //~ int j;
    //~ for (int i = 0; i < len_s; ++i) {
        //~ if (len_t > len_s - i) {
            //~ return NULL;
        //~ }
        //~ for (j = 0; j < len_t && s[j] == t[j]; ++j) {
            //~ ;
        //~ }
        //~ if (j == len_t - 1) {
            //~ return s[i];
        //~ }
    //~ }
    //~ return NULL;
//~ }
