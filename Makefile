CC      = gcc
CFLAGS  = -Wall -Wextra -O2 -std=c11 -IS17
LDFLAGS = -lcunit

DIR_SIMPLE = hello_world E3 E5 S3 S4 S5 S6 S7 S8 S9 S10 S14 Z1 Z2 Z3
SRC_SIMPLE = $(shell find $(DIR_SIMPLE) -name *.c)
ELF_SIMPLE = $(SRC_SIMPLE:.c=.elf)

DIR_TEST = E21 E22 E23 E29 S15 S17 S20 S21 S24 Z4 Z5 Z6 Z7 Z8
SRC_TEST = $(shell find $(DIR_TEST) -name *_test.c)
ELF_TEST = $(SRC_TEST:_test.c=.elf)

all: $(ELF_SIMPLE) $(ELF_TEST)

clean:
	rm -f $(ELF_SIMPLE) $(ELF_TEST)

$(ELF_SIMPLE): %.elf: %.c
	$(CC) $(CFLAGS) -o $@ $<

$(ELF_TEST): %.elf: %_test.c %.c
	$(CC) $(CFLAGS) -o $@ $< $(LDFLAGS)
